/* ************************************************************************************
* File:    led.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## LED1 - control just LED1 from lesson 1

* ************************************************************************************/

#ifndef __LED1_H
#define __LED1_H


void led_setup(void) {
	// set both the cathode and anode pins of the first LED as output
    PORTA.DIRSET = PIN1_bm | PIN2_bm;
}

void led_on(void) {
	PORTA.OUTSET = PIN2_bm;
}

void led_off(void) {
	PORTA.OUTCLR = PIN2_bm;
}

void led_toggle(void) {
	PORTA.OUTTGL = PIN2_bm;
}



#endif // __LED1_H
