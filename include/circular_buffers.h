/* ************************************************************************************
* File:    usart_buffers.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## usart_buffers.h - sample implementation of circular buffers used for USART I/O

* ************************************************************************************/

#pragma once

/* ************************************************************************************
Circular Buffer code copied from uart.h from: https://gitlab.com/bradanlane/ecccore_avr
** ********************************************************************************* */

// cBuffer structure
typedef struct struct_cBuffer {
	unsigned char *data;		  // the physical memory for the buffer
	uint16_t size;				  // the allocated size of the buffer
	uint16_t length;			  // the length of the data currently in the buffer
	uint16_t current;			  // the index into the buffer where the data starts
} cBuffer;

#define CRITICAL_SECTION_START	cli()
#define CRITICAL_SECTION_END	sei()

void bufferReset(cBuffer *buffer, uint8_t *data, uint16_t size) {
	// begin critical section
	CRITICAL_SECTION_START;
	buffer->data = data;
	buffer->size = size;
	// initialize index and length
	buffer->current = 0;
	buffer->length = 0;
	// end critical section
	CRITICAL_SECTION_END;
}

// access routines
int bufferPeek(cBuffer *buffer) {
	int rtn = -1;
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if there's data in the buffer
	if (buffer->length) {
		// get the first character from buffer
		rtn = buffer->data[buffer->current];
	}
	// end critical section
	CRITICAL_SECTION_END;
	// return
	return rtn;
}

// access routines
int bufferGet(cBuffer *buffer) {
	int rtn = -1;
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if there's data in the buffer
	if (buffer->length) {
		// get the first character from buffer
		rtn = buffer->data[buffer->current];
		// move index down and decrement length; wraps around at end of buffer
		buffer->current++;
		if (buffer->current >= buffer->size)
		buffer->current -= buffer->size;
		buffer->length--;
	}
	// end critical section
	CRITICAL_SECTION_END;
	// return
	return rtn;
}

int bufferPut(cBuffer *buffer, uint8_t data) {
	int rtn = -1;

	// begin critical section
	CRITICAL_SECTION_START;
	// make sure the buffer has room
	if (buffer->length < buffer->size) {
		// save data byte at end of buffer
		buffer->data[(buffer->current + buffer->length) % buffer->size] = data;
		// increment the length
		buffer->length++;
		rtn = data;
	}
	// end critical section
	CRITICAL_SECTION_END;
	// return the original data character or failure
	return rtn;
}

int bufferPutWait(cBuffer *buffer, uint8_t data) {
	int rtn = -1;
	uint8_t attempts = 50;	// 50 * 5 ms = 250ms is the maximum we will wait to load the data
	while ((rtn = bufferPut(buffer, data)) < 0) {
		attempts--;
		if (!attempts)
		break;
		milliseconds_delay(5);
	}
	return rtn;
}

unsigned short bufferCapacity(cBuffer *buffer) {
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if the buffer has room
	// return true if there is room
	unsigned short bytesleft = (buffer->size - buffer->length);
	// end critical section
	CRITICAL_SECTION_END;
	return bytesleft;
}

unsigned short bufferEmpty(cBuffer *buffer) {
	// begin critical section
	CRITICAL_SECTION_START;
	// check to see if the buffer has room
	// return true if there is room
	unsigned short empty = true;
	if (buffer->length)
	empty = false;
	// end critical section
	CRITICAL_SECTION_END;
	return empty;
}

void bufferFlush(cBuffer *buffer) {
	// begin critical section
	CRITICAL_SECTION_START;
	// flush contents of the buffer
	buffer->length = 0;
	// end critical section
	CRITICAL_SECTION_END;
}

// uart structure
typedef struct struct_uart {
	uint8_t id;					// which usart (0 or 1); we track this to optimize some code
	cBuffer buffer1; 			// UART receive buffer
	cBuffer buffer2; 			// UART transmit buffer
	unsigned short rxOverflow;	// receive overflow counter
	uint8_t txIdle;				// the TX buffer was emptied and the interrupt will need a jump start
} uartObj;


// receive and transmit buffers
#ifndef BUFFER1_SIZE
#define BUFFER1_SIZE 0
#endif
#ifndef BUFFER2_SIZE
#define BUFFER2_SIZE 0
#endif

#if BUFFER1_SIZE > 0
static uint8_t bufferData1[BUFFER1_SIZE];
#endif
#if BUFFER2_SIZE > 0
static uint8_t bufferData2[BUFFER2_SIZE];
#endif

static uartObj uart;

/* ******************************************************************************************
All of the above code was copied from uart.h from: https://gitlab.com/bradanlane/ecccore_avr
** *************************************************************************************** */

void buffers_init(void) {
	// initialize the buffers
#if BUFFER1_SIZE > 0
	bufferReset(&(uart.buffer1), bufferData1, BUFFER1_SIZE); // initialize the UART receive buffer
#endif
#if BUFFER2_SIZE > 0
	bufferReset(&(uart.buffer2), bufferData2, BUFFER2_SIZE); // initialize the UART transmit buffer
#endif
}


#if BUFFER2_SIZE > 0

int buffers_tx_get_byte(void) {
	return bufferGet(&(uart.buffer2));
}

int buffers_tx_peek_byte(void) {
	return bufferPeek(&(uart.buffer2));
}

int buffers_tx_put_byte(uint8_t b) {
	return bufferPut(&(uart.buffer2), b);
}

uint16_t buffers_tx_get_length(void) {
	return uart.buffer2.length;
}

void buffers_tx_flush(void) {
	bufferFlush(&(uart.buffer2));
}

// to be more agnostic, we allow the buffers to be used for any purpose
// in this scenario, we may not want to refer to them with 'rx' or 'tx'
// when both buffers are used, they may be referenced as buffer1_*() and buffer2_*()
// when there is only a single buffer, it may be referenced as buffer_*()
#if (BUFFER1_SIZE > 0)
#define buffer2_get_byte buffers_tx_get_byte
#define buffer2_peek_byte buffers_tx_peek_byte
#define buffer2_put_byte buffers_tx_put_byte
#define buffer2_get_length buffers_tx_get_length
#define buffer2_flush buffers_tx_flush
#else
#define buffer_get_byte buffers_tx_get_byte
#define buffer_peek_byte buffers_tx_peek_byte
#define buffer_put_byte buffers_tx_put_byte
#define buffer_get_length buffers_tx_get_length
#define buffer_flush buffers_tx_flush
#endif

#endif	// BUFFER2_SIZE > 0


#if BUFFER1_SIZE > 0

int buffers_rx_get_byte(void) {
	return bufferGet(&(uart.buffer1));
}

int buffers_rx_peek_byte(void) {
	return bufferPeek(&(uart.buffer1));
}

int buffers_rx_put_byte(uint8_t b) {
	return bufferPut(&(uart.buffer1), b);
}

uint16_t buffers_rx_get_length(void) {
	return uart.buffer1.length;
}

void buffers_rx_flush(void) {
	bufferFlush(&(uart.buffer1));
}

// to be more agnostic, we allow the buffers to be used for any purpose
// in this scenario, we may not want to refer to them with 'rx' or 'tx'
// when both buffers are used, they may be referenced as buffer1_*() and buffer2_*()
// when there is only a single buffer, it may be referenced as buffer_*()
#if (BUFFER2_SIZE > 0)
#define buffer1_get_byte buffers_rx_get_byte
#define buffer1_peek_byte buffers_rx_peek_byte
#define buffer1_put_byte buffers_rx_put_byte
#define buffer1_get_length buffers_rx_get_length
#define buffer1_flush buffers_rx_flush
#else
#define buffer_get_byte buffers_rx_get_byte
#define buffer_peek_byte buffers_rx_peek_byte
#define buffer_put_byte buffers_rx_put_byte
#define buffer_get_length buffers_rx_get_length
#define buffer_flush buffers_rx_flush
#endif

#endif	// BUFFER1_SIZE > 0
