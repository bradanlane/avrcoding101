/* ************************************************************************************
* File:    eeprom.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## EEPROM - read/write to EEPROM memory

* ************************************************************************************/

#ifndef __EEPROM_H
#define __EEPROM_H


void eeprom_put_byte(uint16_t relative_addr, uint8_t data) {
	if (relative_addr < EEPROM_SIZE) {

		/* Wait for completion of any previous write */
		while (NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm)
			;

		// Load in the relevant EEPROM page by writing directly to the memory address
		*(uint8_t *)(EEPROM_START + relative_addr) = data;

		// Unlock self programming and then erase/write the 'page' (one byte)
		CCP = CCP_SPM_gc;
		NVMCTRL.CTRLA = NVMCTRL_CMD_PAGEERASEWRITE_gc;
	}
}

uint8_t eeprom_get_byte(uint16_t relative_addr) {
	uint8_t b = 0;

	if (relative_addr < EEPROM_SIZE) {
		// Read operation will be stalled by hardware if any write is in progress
		b = *(uint8_t *)(EEPROM_START + relative_addr);
	}

	return b;
}

#endif // __EEPROM_H
