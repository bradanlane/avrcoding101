/* ************************************************************************************
 * File:    fish.h
 * Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## Music demo data - notes and lyrics for 'So Long and Thanks for All the Fish'

* ************************************************************************************/

#ifndef __FISH_H
#define __FISH_H

#define FISH_SIGNATURE	4	// 4/4 but some scores are also cut time
#define FISH_TRANSPOSE	0
#define FISH_STACCATO	true
#define FISH_TEMPO 		155	// override default temp

const uint8_t fish_song[] = {

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_C, 2, EIGHTH),
	D(NOTE_G, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),

	N(NOTE_D, 1, QUARTER),
	N(NOTE_C, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_E, 1, EIGHTH),
	N(NOTE_C, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_C, 2, QUARTER),
	N(NOTE_G, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),

	N(NOTE_D, 1, QUARTER),
	N(NOTE_C, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_E, 1, EIGHTH),
	N(NOTE_C, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_C, 1, QUARTER),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_C, 1, QUARTER),

	N(NOTE_G, 1, QUARTER),
	N(NOTE_F, 1, EIGHTH),
	D(NOTE_C, 1, QUARTER),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_E, 1, WHOLE), // actually should be eighth tied to a whole; we just do a whole and an eighth rest
	N(NOTE_REST, 0, EIGHTH),

	N(NOTE_REST, 0, WHOLE),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_C, 2, EIGHTH),
	D(NOTE_G, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),

	N(NOTE_D, 1, QUARTER),
	N(NOTE_C, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_E, 1, EIGHTH),
	N(NOTE_C, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_C, 2, QUARTER),
	N(NOTE_C, 2, QUARTER),
	N(NOTE_G, 1, QUARTER),

	N(NOTE_F, 1, QUARTER),
	N(NOTE_E, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_E, 1, EIGHTH),
	N(NOTE_C, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_C, 1, QUARTER),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_C, 1, QUARTER),

	N(NOTE_G, 1, QUARTER),
	N(NOTE_F, 1, EIGHTH),
	N(NOTE_C, 1, QUARTER),
	N(NOTE_C, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),

	N(NOTE_E, 1, HALF),
	N(NOTE_G, 1, HALF),

	N(NOTE_A, 1, HALF),
	N(NOTE_C, 2, HALF),

	N(NOTE_G, 1, EIGHTH),
	N(NOTE_C, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_G, 1, EIGHTH),
	N(NOTE_C, 2, EIGHTH),
	N(NOTE_G, 1, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_C, 2, EIGHTH),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_C, 1, EIGHTH),
	N(NOTE_C, 1, QUARTER),
	N(NOTE_B, 0, EIGHTH),
	N(NOTE_C, 1, WHOLE), // actually should be eighth tied to a whole; we just do a whole and an eighth rest
	N(NOTE_REST, 0, EIGHTH),

	N(NOTE_REST, 0, WHOLE),

	// key change

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_D, 2, EIGHTH),
	D(NOTE_A, 1, QUARTER),
	N(NOTE_FS, 1, QUARTER),

	N(NOTE_E, 1, QUARTER),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_E, 1, QUARTER),
	N(NOTE_FS, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_D, 2, QUARTER),
	N(NOTE_D, 2, QUARTER),
	N(NOTE_A, 1, QUARTER),

	N(NOTE_G, 1, EIGHTH),
	N(NOTE_FS, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),
	N(NOTE_FS, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),
	N(NOTE_D, 1, QUARTER),

	N(NOTE_A, 1, EIGHTH),
	D(NOTE_G, 1, QUARTER),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_E, 1, EIGHTH),
	N(NOTE_FS, 1, WHOLE), // actually should be eighth tied to a whole; we just do a whole and an eighth rest
	N(NOTE_REST, 0, EIGHTH),

	N(NOTE_REST, 0, WHOLE),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_D, 2, EIGHTH),
	D(NOTE_A, 1, QUARTER),
	N(NOTE_FS, 1, QUARTER),

	N(NOTE_E, 1, QUARTER),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_E, 1, QUARTER),
	N(NOTE_FS, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_D, 2, QUARTER),
	D(NOTE_D, 2, QUARTER),
	N(NOTE_D, 1, QUARTER),

	N(NOTE_G, 1, EIGHTH),
	N(NOTE_FS, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_FS, 1, QUARTER),

	N(NOTE_REST, 0, QUARTER),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_E, 1, QUARTER),
	N(NOTE_D, 1, QUARTER),

	N(NOTE_A, 1, QUARTER),
	N(NOTE_G, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_E, 1, QUARTER),

	N(NOTE_FS, 1, HALF),
	N(NOTE_A, 1, HALF),

	N(NOTE_B, 1, HALF),
	N(NOTE_D, 2, HALF),

	N(NOTE_A, 1, EIGHTH),
	N(NOTE_D, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_A, 1, EIGHTH),
	N(NOTE_D, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_A, 1, EIGHTH),
	N(NOTE_D, 2, EIGHTH),

	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_A, 1, EIGHTH),
	N(NOTE_D, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_A, 1, EIGHTH),
	N(NOTE_D, 2, EIGHTH),
	N(NOTE_REST, 0, QUARTER),

	N(NOTE_A, 1, EIGHTH),
	N(NOTE_E, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_A, 1, EIGHTH),
	N(NOTE_E, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_A, 1, EIGHTH),
	N(NOTE_DS, 2, EIGHTH),

	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_B, 1, EIGHTH),
	N(NOTE_DS, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_B, 1, EIGHTH),
	N(NOTE_DS, 2, EIGHTH),
	N(NOTE_REST, 0, QUARTER),

	N(NOTE_A, 1, EIGHTH),
	N(NOTE_D, 2, EIGHTH),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_A, 1, EIGHTH),
	N(NOTE_D, 2, EIGHTH),
	D(NOTE_E, 2, QUARTER),

	N(NOTE_D, 2, QUARTER),
	N(NOTE_REST, 0, EIGHTH),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_D, 1, QUARTER),
	N(NOTE_D, 1, EIGHTH),
	N(NOTE_D, 1, WHOLE), // actually should be eighth tied to a whole; we just do a whole and an eighth rest
	N(NOTE_REST, 0, EIGHTH),
};

// WARNING: must be same number of elements as song
const char *fish_lyrics[] = {
	"\nSo", " long", " and", " thanks", " for", " all", " the", " fish\n",
	"So", " sad", " that", " it", " should", " come", " to", " this\n",
	"We", " tried", " to", " warn", " you", " all", " but", " oh dear\n",
	"\nYou", " may", " not", " share", " our", " in", "tel", "lect\n",
	"Which", " might", " exp", "lain", " your", " dis", "re", "spect\n",
	"For", " all", " the", " na", "tural", " won", "ders", " that\n",
	"grow", " a", "round", " you\n",
	"\nSo", " long,", " so", " long", " and", " thanks\n",
	"for", " all", " the", " fish\n",
	"\nThe", " world's", " a", "bout", " to", " be", " de", "stroyed\n",
	"There's", " no", " point", " get", "ting", " all", " an", "noyed\n",
	"Lie", " back", " and", " let", " the", " planet", " dis", "solve", " (around you)\n",
	"\nDe", "spite", " those", " nets", " of", " tu", "na", " fleets\n",
	"We", " thought", " that", " most", " of", " you", " were", " sweet\n",
	"E", "spe", "cial", "ly", " tiny", " tots", " and", " your\n",
	"preg", "nant", " wo", "men\n",
	"\nSo", " long,", " so", " long,", " so", " long,", " so", " long,", " so", " long\n",
	"So", " long,", " so", " long,", " so", " long,", " so", " long,", " so", " long\n",
	"So", " long,", " so", " long", " and", " thanks\n",
	"for", " all", " the", " fish\n\n"
};

#endif // __FISH_H
