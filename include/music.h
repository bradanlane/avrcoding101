/* ************************************************************************************
 * File:    music.h
 * Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## Music demo using the tone code from lesson 4

* ************************************************************************************/

#ifndef __MUSIC_H
#define __MUSIC_H

// notes: C    C#   D    D#   E    F    F#   G    G#   A    A#   B
const uint16_t _music_scale[] = {0, FREQ_C4, FREQ_C4S, FREQ_D4, FREQ_D4S, FREQ_E4, FREQ_F4, FREQ_F4S, FREQ_G4, FREQ_G4S, FREQ_A4, FREQ_A4S, FREQ_B4};

#define NOTE_0		0
#define NOTE_REST	NOTE_0
#define NOTE_C		1
#define NOTE_CS		2
#define NOTE_DF		NOTE_CS
#define NOTE_D		3
#define NOTE_DS		4
#define NOTE_EF		NOTE_DS
#define NOTE_E		5
#define NOTE_F		6
#define NOTE_ES		NOTE_F
#define NOTE_FS		7
#define NOTE_G		8
#define NOTE_GS		9
#define NOTE_A		10
#define NOTE_AS		11
#define NOTE_BF		NOTE_AS
#define NOTE_B		12
#define NOTE_DOTTED 14
// #define NOTE_TRIPLET	15

// macro to pack notes
#define N(n, o, d)					 ((((d)&0x03) << 6) | (((o)&0x03) << 4) | ((n)&0x0F))
#define D(n, o, d)					 N(NOTE_DOTTED, o, d), N(n, 0, 0)
#define T(n1, o1, n2, o2, n3, o3, d) N(NOTE_TRIPLET, o, d), N(n1, o1, 0), N(n2, o2, 0), N(n3, o3, 0)

// macros to unpack notes
#define GET_NOTE(n)			(((n)&0x0F))
#define GET_OCTAVE(n)		(((n)&0x30) >> 4)
#define GET_DURATION_RAW(n) (((n)&0xC0) >> 6)

// basic music note durations (no triplets dotted notes, etc.
#define WHOLE	0x03 // 0b11
#define HALF	0x02 // 0b10
#define QUARTER 0x01 // 0b01
#define EIGHTH	0x00 // 0b00

void _note_on(uint8_t note, uint8_t octave) {
	if (note > 12) {
		note = 12; // this is a safety but will generate bad notes if 'note' is not between 1..12
	}

	uint16_t frequency = _music_scale[note];
	frequency = frequency << octave;
	tone_on(frequency);
}

uint16_t _music_index;
uint16_t _music_length;
uint32_t _music_note_duration;
uint16_t _music_note_gap;
const uint8_t *_music_notes;

// to save space, these could be defines
uint8_t _music_transpose;
uint8_t _music_tempo;
bool _music_staccato;
uint8_t _music_signature;

void music_setup(const uint8_t *song, uint16_t length, uint8_t signature, uint8_t tempo, uint8_t transpose, bool staccato) {
	_music_notes = song;
	_music_length = length;
	_music_index = _music_length; // set to end of song to prevent playing
	_music_note_duration = 0;
	_music_note_gap = 0;

	_music_signature = signature;
	_music_transpose = transpose;
	_music_tempo = tempo;
	_music_staccato = staccato;
}

void music_start() {
	_music_index = 0; // next note to play is the first note
}

void music_stop() {
	_music_index = _music_length - 1; // next note to play is the last note
}

// must be called often to allow music to progress
int music_continue(void) {
	uint8_t n;

	// end of song; return -1
	if (_music_index >= _music_length) {
		return -1;
	}

	// current note is still playing; noting to do; return 0
	if (_music_note_duration > milliseconds()) {
		return 0;
	}

	// note has finished, wait for gap to finish; return 0
	if (_music_note_gap) {
		tone_off();
		_music_note_duration += _music_note_gap;
		_music_note_gap = 0;
		// return 0 if still nothing to do
		return 0;
	}

	// note and gap have finished; process next note
	n = _music_notes[_music_index];
	_music_index++;

	// unpack the note
	uint8_t note = GET_NOTE(n);
	uint8_t octave = GET_OCTAVE(n) + _music_transpose;
	uint8_t duration = GET_DURATION_RAW(n);

	// need to map the raw duration bits to a value
	// if the music needs sixteenth notes, then its up to the caller to double the temp and sacrifice the whole notes
	switch (duration) {
		case WHOLE: {
			duration = 2 * _music_signature;
		} break;
		case HALF: {
			duration = 4;
		} break;
		case QUARTER: {
			duration = 2;
		} break;
		case EIGHTH:
		default: {
			duration = 1;
		} break;
	}

	uint16_t duration_ms = duration * _music_tempo;

	// if this is a dotted note, then we extend the duration by 50%
	if (note == NOTE_DOTTED) {
		duration_ms += (duration_ms / 2);
		// get the actual note from the next byte in the song
		n = _music_notes[_music_index];
		_music_index++;
		note = GET_NOTE(n);
	}

	if (_music_staccato)
		duration_ms -= 10;

	_music_note_duration = milliseconds() + duration_ms;
	_music_note_gap = (duration_ms * 5) / _music_tempo;
	if (_music_staccato)
		_music_note_gap += 10;

	// led on for notes but not for rests
	if (!note)
		return 0;

	_note_on(note, octave);
	return duration_ms;
}

#endif // __MUSIC_H
