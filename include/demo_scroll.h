/* ************************************************************************************
* File:    final.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## demo.h - put all the lessons together from the AVR Coding 101 Class

Animate charlieplexed LEDs, in time to the music, while sending the lyrics over Serial.
... also, processing Serial I/O !

Note: This file uses all of the modules from "AVR Coding 101".
It also has a few fun additions (music.h and fish.h)

PlatformIO generates it's own make process, hidden to the end user.
This means any `'c` file it finds, is compiled and attempted to be
used in the final program. This may not be desirable - especially
when writing reusable microcontroller code. A convenient alternative,
is to make those file `.h` and include only those needed into `main.c`.

All of this is a choice left to the developer.

* ************************************************************************************/
#define F_CPU 20000000L

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "cpu.h"
#include "buttons.h"
#include "leds6x7.h"
#include "milliseconds.h"
#include "scroll.h"
#include "tone.h"
#include "music.h"
#include "fish.h"
#include "usart.h"
#include "eeprom.h"

void output_serial_number(void) {
	uint8_t byte, half;
	const uint8_t *serno = &(SIGROW_SERNUM0);

	// read and output the serial number
	for (int i = 0; i < 10; i++) {
		byte = serno[i];

		half = (byte >> 4) & 0x0F;
		if (half > 9)
			half = 'A' + (half - 10);
		else
			half = '0' + (half);
		usart_put_byte(half);

		half = (byte)&0x0F;
		if (half > 9)
			half = 'A' + (half - 10);
		else
			half = '0' + (half);
		usart_put_byte(half);

		usart_put_byte(' ');
	}

	usart_put_byte('\n');
}

const char message1[] = "AVR Coding 101";
const char message2[] = "So Long and Thanks for all the Fish";

int main(void) {
	cpu_disable_clock_prescaler();

	milliseconds_setup();
	usart_setup(115200);
	leds_setup();
	buttons_setup();
	tone_setup();
	music_setup(fish_song, sizeof(fish_song), FISH_SIGNATURE, FISH_TEMPO, FISH_TRANSPOSE, FISH_STACCATO);

	uint32_t beat_timer = 0;
	uint8_t beat_state = 0; // mini state machine 'state'

	uint16_t lyric = 0;
	uint16_t lyrics_length = sizeof(fish_lyrics) / sizeof(char *);

	// simple little test of LEDs and buzzer
	leds_bitmap_fill();
	tone_on(FREQ_A4);
	milliseconds_delay(1000);
	tone_off();

	bool pressed = false;

	usart_put_byte('\n');

	scroll_setup(message1, 50, true);

	while (1) {
		int music = 0;

		// update the song (it handles when to start the next note)
		music = music_continue();
		scroll_message();

		if (music > 0) { // start of a new note
			// a new note has begun
			// output the lyrics
			if (lyric < lyrics_length) {
				usart_put_string(fish_lyrics[lyric]);
				lyric++;
			}
		} else if (music == 0) { // continuation of current note
			// do nothing
		} else if (music < 0) { // music has ended
			if (lyric > 0) {
				// switch back to message 1
				lyric = 0;
				scroll_setup(message1, 50, true);
			}
		}

		// check the second button
		if (button2_pressed()) {
			if (!pressed) {
				pressed = true;

				if (music < 0) {
					// restart the song
					lyric = 0; // start lyrics from the beginning
					scroll_setup(message2, 50, true);
					music_start();
				}
			}
		} else if (button1_pressed()) {
			if (!pressed) {
				pressed = true;
				output_serial_number();
			}

		} else {
			if (pressed)
				pressed = false;
		}

		// receive, transform, and resend any serial data
		if (usart_data_available()) {
			int data = 0;
			data = usart_get_byte(); // get a byte
			if (data >= 0) {
				if ((data >= 'a') && (data <= 'z'))
					data = data - ('a' - 'A'); // upper case a lower case character
				else if ((data >= 'A') && (data <= 'Z'))
					data = data + ('a' - 'A'); // lowercase case an upper case character

				usart_put_byte((uint8_t)data); // echo the data back to the sender
			}
		}
	}

	return 0;
}
