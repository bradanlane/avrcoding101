/* ************************************************************************************
* File:    scroll.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## SCROLL - scroll a text message on the LEDs

* ************************************************************************************/

#pragma once

// the ACK1 LEDs can display 6 columns; at any given time we may have at most portions of 2 5x7 characters
typedef union _SCROLLBUFFER {
	uint16_t value;
	uint8_t blocks[2]; // little-endian means led[0] == (value & 0xFF000000)
} SCROLLBUFFER;

SCROLLBUFFER _scroll_buffer[ROW_COUNT];

const uint8_t *get_letter_bitmap(char c) {
	// warning: no error handling
	c = c - 32;					// font does not contain for 32 glyphs
	return (&(font7x5[c * 7])); // each glyph is 7 bytes
}

static uint32_t _scroll_timer = 0;
static uint8_t _scroll_counter = 0;
static uint8_t _scroll_index = 0;
static uint16_t _scroll_speed = 0;
static const char *_scroll_message = NULL;
static bool _scroll_repeat = false;
static bool _scroll_enabled = false;

void scroll_stop(void) {
	_scroll_enabled = false;
	_scroll_speed = 0;
	_scroll_repeat = false;
	_scroll_timer = 0;
	_scroll_counter = 6; // columns in a font
	_scroll_message = NULL;
	_scroll_index = 0;	 // first character in the message
}

void scroll_setup(const char *msg, uint16_t speed, bool repeat) {
	if (!msg || (msg[0] == 0)) {
		scroll_stop();
		return;
	}

	if (speed == 0) speed = 125;

	_scroll_enabled = true;
	_scroll_speed = speed;
	_scroll_repeat = repeat;
	_scroll_timer = milliseconds() + _scroll_speed;
	_scroll_counter = 6; // columns in a font
	_scroll_message = msg;
	_scroll_index = 0;	 // first character in the message

	char *glyph = (char*)(get_letter_bitmap(_scroll_message[_scroll_index]));
	for (uint8_t i = 0; i < ROW_COUNT; i++) {
		_scroll_buffer[i].value = 0;			// set all blocks to zeros
		_scroll_buffer[i].blocks[1] = glyph[i]; // set second block to character
	}
}

void scroll_message(void) {
	if (!_scroll_enabled)
		return;

	if (!_scroll_message[0]) {
		// if our scroll text disappears, stop trying to scroll
		_scroll_enabled = false;
		leds_bitmap_clear();
		return;
	}

	if (_scroll_timer < milliseconds()) {
		_scroll_timer = milliseconds() + _scroll_speed;

		// if the counter reaches zero, load next character
		if (_scroll_counter == 0) {
			char *glyph;

			if (_scroll_message[_scroll_index] == 0) {
				if (_scroll_repeat)
					_scroll_index = 0; // start message over again
			} else
				_scroll_index++;	   // next character in the message

			if (_scroll_message[_scroll_index] == 0) {
				glyph = (char*)(get_letter_bitmap(' '));
				_scroll_counter = 12; // double space for extended gap
			} else {
				glyph = (char*)(get_letter_bitmap(_scroll_message[_scroll_index]));
				_scroll_counter = 6;
			}

			for (uint8_t i = 0; i < ROW_COUNT; i++) {
				_scroll_buffer[i].blocks[1] = glyph[i]; // set second block to character
			}
		}
		uint8_t buffer[ROW_COUNT];
		for (uint8_t i = 0; i < ROW_COUNT; i++) {
			_scroll_buffer[i].value = _scroll_buffer[i].value >> 1; // scroll both blocks
			buffer[i] = _scroll_buffer[i].blocks[0];				// copy first block to buffer
		}
		leds_bitmap_load(buffer);
		_scroll_counter--; // decrement character column counter
	}
}
