/* ************************************************************************************
 * File:    usart_tx_only
 .h
 * Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## USART_TX_ONLY - Simple Serial output without interrupts from lesson 5

* ************************************************************************************/

#ifndef __USART_TX_ONLY_H
#define __USART_TX_ONLY_H

// this formula appears different from the one published by Microchip(r)
// it avoids floating point math while generating the same result
#define BAUD_RATE_FORMULA(b) ((((8 * F_CPU) / b) + 1) / 2)

void usart_setup(uint32_t baudrate) {
	// cli(); // disable all interrupt handlers

	// set  baud rate
	USART0.BAUD = (uint16_t)BAUD_RATE_FORMULA(baudrate);

	// the default USART pins are 2 and 3 on PORTB
	PORTB.DIRSET = PIN2_bm; // TX pin is output
	PORTB.DIRCLR = PIN3_bm; // RX pin is input

	USART0.CTRLB |= USART_TXEN_bm; // enable TX
}

/* ---
transmit a byte
-- */
int usart_put_byte(uint8_t data) {
	// wait for any previous TX to be finished
	while (!(USART0.STATUS & USART_DREIF_bm)) {
		;
	}
	// put new byte into the TX data register
	USART0.TXDATAL = data;
	return data;
}

/* ---
transmit a string one byte at a time
-- */
void usart_put_string(const char *text) {
	// no error handling
	for (int i = 0; i < strlen(text); i++)
		usart_put_byte((uint8_t)text[i]);
}

#endif // __USART_TX_ONLY_H
