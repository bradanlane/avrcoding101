/* ************************************************************************************
 * File:    usart.h
 * Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## USART - Serial I/O from lesson 5

* ************************************************************************************/

#ifndef __USART_H
#define __USART_H

/* 	Buffer sizes:
	the TX buffer can be small since you could write the 'put' code to wait
	to send all the data; it could be just a few bytes
	the RX buffer should be large enough to receive a 'blast' of data
	it's also good to process RX data as quickly as possible to avoid overflow

	Note: to simplify the lesson, the circular buffer code has been provided.
	The circular buffer code has been pulled from ecccore_avr (under MIT license).

	The important functions are:

		void buffers_init(void);
		int buffers_tx_get_byte(void);
		int buffers_tx_put_byte(uint8_t b);
		uint16_t buffers_tx_get_length(void);
		int buffers_rx_get_byte(void);
		int buffers_rx_put_byte(uint8_t b);
		uint16_t buffers_rx_get_length(void);
*/

#define BUFFER1_SIZE 64
#define BUFFER2_SIZE 128
#include "circular_buffers.h"

// this 'idle' flag allows the TX interrupt to be enabled/disabled as needed
uint8_t _usart_tx_idle; // the TX buffer was emptied and the interrupt will need a jump start

// the overflow counter is for error handling; it not used in the lesson
uint8_t _usart_rx_overflow; // 'RX buffer is full' counter

// this macros exist only to make the code more readable
#define USART_ENABLE_TX 	{ USART0.CTRLA |= (USART_DREIE_bm); }
#define USART_DISABLE_TX 	{ USART0.CTRLA &= ~(USART_DREIE_bm); }
#define USART_ENABLE_RX 	{ USART0.CTRLA |= (USART_RXCIE_bm); }
#define USART_DISABLE_RX 	{ USART0.CTRLA &= ~(USART_RXCIE_bm); }

// USART Transmit Complete Interrupt Handler
ISR(USART0_DRE_vect) {
	// "data register empty", send the next byte
	if (buffers_tx_get_length()) {
		// send byte from top of buffer
		USART0.TXDATAL = (unsigned char)buffers_tx_get_byte();
	} else {
		// buffer is empty, turn off interrupt
		_usart_tx_idle = true;
		USART_DISABLE_TX;
	}
}

// USART Receive Complete Interrupt Handler
ISR(USART0_RXC_vect) {
	unsigned char c;

	// get received char
	c = USART0.RXDATAL; // this also clears the interrupt flag for us

	// put received char in circular buffer
	// check if there's space
	if (buffers_rx_put_byte(c) < 0) // -1 means the 'put' failed
		_usart_rx_overflow++;	 // no space in buffer
	else
		_usart_rx_overflow = 0;
}

// this formula appears different from the one published by Microchip(r)
// it avoids floating point math while generating the same result
#define BAUD_RATE_FORMULA(b) ((((8 * F_CPU) / b) + 1) / 2)

void usart_setup(uint32_t baudrate) {
	buffers_init();

	// cli(); // disable all interrupt handlers

	// set  baud rate
	uint16_t interval = (uint16_t)BAUD_RATE_FORMULA(baudrate);
	interval = CLK_COMPENSATION(interval);
	USART0.BAUD = interval;

	// the default USART pins are 2 and 3 on PORTB
	PORTB.DIRSET = PIN2_bm; // TX pin is output
	PORTB.DIRCLR = PIN3_bm; // RX pin is input

	USART0.CTRLB |= (USART_TXEN_bm) | (USART_RXEN_bm); // enable TX and RX

	USART_ENABLE_RX; // enable RX interrupt
	// the TX interrupt is enables only when there is data to send
	_usart_tx_idle = true;

	_usart_rx_overflow = 0;

	sei(); // enable all interrupt handlers
}


bool usart_data_available(void) {
	if (buffers_rx_get_length())
		return true;
	return false;
}

/* ---
gets a single byte from the receive buffer (getchar-style) returns -1 if no byte is available
--- */
int usart_get_byte() {
	// get a byte from the circular buffer of RX data
	return buffers_rx_get_byte();
}

/* ---
transmit a byte
-- */
int usart_put_byte(uint8_t data) {
	// put a byte onto the circular buffer for TX data
	int rtn = buffers_tx_put_byte(data);

	if (rtn >= 0) {
		// if we have been idle, jump-start the transmission
		if (_usart_tx_idle && buffers_tx_get_length()) {
			_usart_tx_idle = false;
			USART_ENABLE_TX;
		}
	} else {
		// there was a problem
	}

	return rtn;
}

/* ---
transmit a string one byte at a time
-- */
void usart_put_string(const char *text) {
	// no error handling
	for (int i = 0; i < strlen(text); i++)
		usart_put_byte((uint8_t)text[i]);
}

#endif // __USART_H
