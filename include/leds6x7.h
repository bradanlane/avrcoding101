/* ************************************************************************************
* File:    leds6x7.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## charlieplex LEDs - LED display buffer routines from lesson 3

manipulate and buffer representing all of the LEDs
provide inline code (used by the timer) for updating the LEDs

IMPORTANT: when using this file, it must be included *before* milliseconds.h

* ************************************************************************************/

#ifndef __LEDS_H
#define __LEDS_H


// this definition makes the code easier to read
#define ALL_LED_PINS (PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm)

// these definitions are used to iterate through all of the LEDs
#define COL_COUNT 6
#define ROW_COUNT 7

// arrays represent the LEDs from left to right and top to bottom

const uint8_t _led_anodes[COL_COUNT * ROW_COUNT] = {
	PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm};
const uint8_t _led_cathodes[ROW_COUNT] = {
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm};

uint8_t _led_bitmap[ROW_COUNT]; // a unit8_t supports up to 8 LEDs in a single row

void led_on(uint8_t num) {
	num = num % (COL_COUNT * ROW_COUNT);
	uint8_t col = num % COL_COUNT;
	uint8_t row = num / COL_COUNT;
	_led_bitmap[row] |= (1 << col);
}

// set the bit in the display buffer to 0 so the LED will off when refreshed
void led_off(uint8_t num) {
	num = num % (COL_COUNT * ROW_COUNT);
	uint8_t col = num % COL_COUNT;
	uint8_t row = num / COL_COUNT;
	_led_bitmap[row] &= ~(1 << col);
}

// toggle the bit in the display buffer between 1 and 0 so the LED will be the opposite when refreshed
void led_toggle(uint8_t num) {
	num = num % (COL_COUNT * ROW_COUNT);
	uint8_t col = num % COL_COUNT;
	uint8_t row = num / COL_COUNT;
	if (_led_bitmap[row] & (1 << col))
		_led_bitmap[row] &= ~(1 << col);
	else
		_led_bitmap[row] |= (1 << col);
}


void leds_bitmap_fill(void) {
	// when we are doing our setup, we can populate or clear our bitmap
	memset(_led_bitmap, 0xFF, ROW_COUNT); // a '1' bit sets the LED on
}

void leds_bitmap_clear(void) {
	// when we are doing our setup, we can populate or clear our bitmap
	memset(_led_bitmap, 0x00, ROW_COUNT); // a '1' bit sets the LED on
}

void leds_bitmap_load(const uint8_t *bitmap) {
	// warning: no error handling
	memcpy(_led_bitmap, bitmap, ROW_COUNT);
}

#include "font.h"

void leds_bitmap_letter(char c) {
	if ((c < ' ') || (c > 127)) c = ' ';
	c = c - 32;	// font does not contain first 32 glyphs
	uint8_t *bitmap;
	bitmap = (uint8_t *)(&(font7x5[c * 7]));	// each glyph is 7 bytes
	memcpy(_led_bitmap, bitmap, ROW_COUNT);
}


volatile uint8_t _led_active_row;

#define LED_INLINE_CODE	// this lets the milliseconds.h code know to include the update code

static inline void leds_update(uint32_t timer) {
	if ((timer % 2) == 0) {
		//PORTC.OUTSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header

		// we use one global variable: led_row
		uint8_t pins = 0;
		for (uint8_t col = 0; col < COL_COUNT; col++) {
			// we use bitwise bit shifting to test each bit of the bitmap
			if (_led_bitmap[_led_active_row] & (1 << col)) {
				// if the bit is set the we add the anode pin to our collection
				pins |= _led_anodes[(_led_active_row * COL_COUNT) + col];
			}
		}
		// reset all of the LED pins to input
		PORTA.OUTCLR = ALL_LED_PINS;
		PORTA.DIRCLR = ALL_LED_PINS;

        // make all the anodes and the one cathode as output
        PORTA.DIRSET = (pins | _led_cathodes[_led_active_row]);
        // make the anodes as HIGH
        PORTA.OUTSET = pins;
        // increment to the next row for the next update
        _led_active_row = (_led_active_row + 1) % ROW_COUNT;

		//PORTC.OUTCLR = PIN2_bm;	// for debugging we can use PC2 from the breakout header
	}
}

// set everything to a known starting point
void leds_setup(void) {
	_led_active_row = 0;
	leds_bitmap_clear(); // all LEDs off to start
	//PORTC.DIRSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header
}

#endif // __LEDS_H
