/* ************************************************************************************
* File:    final.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## demo.h - put all the lessons together from the AVR Coding 101 Class

Animate charlieplexed LEDs, in time to the music, while sending the lyrics over Serial.
... also, processing Serial I/O !

Note: This file uses all of the modules from "AVR Coding 101".
It also has a few fun additions (music.h and fish.h)

PlatformIO generates it's own make process, hidden to the end user.
This means any `'c` file it finds, is compiled and attempted to be
used in the final program. This may not be desirable - especially
when writing reusable microcontroller code. A convenient alternative,
is to make those file `.h` and include only those needed into `main.c`.

All of this is a choice left to the developer.

* ************************************************************************************/
#define F_CPU 20000000L

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "cpu.h"
#include "buttons.h"
#include "leds6x7.h"
#include "milliseconds.h"
#include "tone.h"
#include "music.h"
#include "fish.h"
#include "usart.h"
#include "eeprom.h"

const uint8_t big_heart[ROW_COUNT] = {0b00010010, 0b00111111, 0b00111111, 0b00111111, 0b00011110, 0b00001100, 0b00000000};
const uint8_t lil_heart[ROW_COUNT] = {0b00000000, 0b00010010, 0b00111111, 0b00011110, 0b00001100, 0b00000000, 0b00000000};

void output_serial_number(void) {
	uint8_t byte, half;
	const uint8_t *serno = &(SIGROW_SERNUM0);

	// read and output the serial number
	for (int i = 0; i < 10; i++) {
		byte = serno[i];

		half = (byte >> 4) & 0x0F;
		if (half > 9)
			half = 'A' + (half - 10);
		else
			half = '0' + (half);
		usart_put_byte(half);

		half = (byte)&0x0F;
		if (half > 9)
			half = 'A' + (half - 10);
		else
			half = '0' + (half);
		usart_put_byte(half);

		usart_put_byte(' ');
	}

	usart_put_byte('\n');
}

int main(void) {
	cpu_disable_clock_prescaler();

	milliseconds_setup();
	usart_setup(115200);
	leds_setup();
	buttons_setup();
	tone_setup();
	music_setup(fish_song, sizeof(fish_song), FISH_SIGNATURE, FISH_TEMPO, FISH_TRANSPOSE, FISH_STACCATO);

	uint32_t beat_timer = 0;
	uint8_t beat_state = 0; // mini state machine 'state'

	uint16_t lyric = 0;
	uint16_t lyrics_length = sizeof(fish_lyrics) / sizeof(char *);

	// simple little test of LEDs and buzzer
	leds_bitmap_fill();
	tone_on(FREQ_A4);
	milliseconds_delay(1000);
	tone_off();

	bool pressed = false;

	usart_put_byte('\n');

#if 0
	for (uint8_t i = 32; i < 127; i++) {
		// an extention of displaying characters is to scroll them
		// by expanding the bitmap buffer used by led update code
		leds_bitmap_letter(i);
		milliseconds_delay(125);
	}
#endif

	while (1) {
		int music = 0;

		// update the song (it handles when to start the next note)
		music = music_continue();

		if (music > 0) { // start of a new note
			// a new note has begun
			// output the lyrics
			if (lyric < lyrics_length) {
				usart_put_string(fish_lyrics[lyric]);
				lyric++;
			}
			// synchronize heart beat
			beat_timer = milliseconds() + (music / 3);
			leds_bitmap_load(lil_heart);
		} else if (music == 0) { // continuation of current note
			if (beat_timer < milliseconds()) {
				leds_bitmap_load(big_heart);
				beat_timer = milliseconds() + 1000; // some longer than needed delay; timer will get changed on next note
			}
		} else if (music < 0) { // music has ended
			if (beat_timer < milliseconds()) {
				// a mini state machine for the heard peat animation and sounds
				switch (beat_state) {
					case 0: { // start short beat
						leds_bitmap_load(lil_heart);
						beat_timer = milliseconds() + 33;
						tone_on(FREQ_C4 / 4);
						beat_state = 1;
					} break;
					case 1: { // end short beat tone
						beat_timer = milliseconds() + 133;
						tone_off();
						beat_state = 2;
					} break;
					case 2: { // long short beat
						leds_bitmap_load(big_heart);
						beat_timer = milliseconds() + 33;
						tone_on(FREQ_C4 / 4);
						beat_state = 3;
					} break;
					case 3: { // end short beat tone
						beat_timer = milliseconds() + 700;
						tone_off();
						beat_state = 0;
					} break;
				}
			}
		}

		// check the second button
		if (button2_pressed()) {
			if (!pressed) {
				pressed = true;

				if (music < 0) {
					// restart the song
					lyric = 0; // start lyrics from the beginning
					music_start();
				}
			}
		} else if (button1_pressed()) {
			if (!pressed) {
				pressed = true;
				output_serial_number();
			}

		} else {
			if (pressed)
				pressed = false;
		}

		// receive, transform, and resend any serial data
		if (usart_data_available()) {
			int data = 0;
			data = usart_get_byte(); // get a byte
			if (data >= 0) {
				if ((data >= 'a') && (data <= 'z'))
					data = data - ('a' - 'A'); // upper case a lower case character
				else if ((data >= 'A') && (data <= 'Z'))
					data = data + ('a' - 'A'); // lowercase case an upper case character

				usart_put_byte((uint8_t)data); // echo the data back to the sender
			}
		}
	}

	return 0;
}
