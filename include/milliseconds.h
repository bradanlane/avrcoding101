/* ************************************************************************************
* File:    milliseconds.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## milliseconds - timer routines from lesson 2

Create a simple 1 millisecond timer with a few convenience routines.

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/

#ifndef __MILLISECONDS_H
#define __MILLISECONDS_H

#define TCB_INTERVAL (((F_CPU) / 1000) - 1)

volatile uint32_t _timer_counter = 0; // global variable holds the millisecond value

ISR(TCB0_INT_vect) {
	// PORTC.OUTTGL = PIN2_bm;	// for debugging we can use PC2 from the breakout header

	// perform 1KHz aka milliseconds tasks
	_timer_counter++;
#ifdef LED_INLINE_CODE // include the LEDs update code
	leds_update(_timer_counter);
#endif
	TCB0.INTFLAGS = TCB_CAPT_bm; // must clear the interrupt flag
}

void milliseconds_setup() {
	uint16_t interval = TCB_INTERVAL;
	interval = CLK_COMPENSATION(interval);

	TCB0.CCMP = interval;			 // set the compare/capture register to our desired value (interval - 1)
	TCB0.INTCTRL = TCB_CAPT_bm;		 // enable the capture interrupt
	TCB0.CTRLB = TCB_CNTMODE_INT_gc; // timer mode: periodic interrupt; this is actually the default
	TCB0.CTRLA = TCB_ENABLE_bm;
	sei(); // enable interrupts

	//PORTC.DIRSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header
}

uint32_t milliseconds(void) {
	uint32_t t;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { t = _timer_counter; }
	return t;
}

void milliseconds_delay(uint16_t ms) {
	uint32_t timer = milliseconds() + ms;
	while (timer > milliseconds()) {
		// spin wheels
	}
}

#endif // __MILLISECONDS_H
