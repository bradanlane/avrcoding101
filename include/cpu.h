/* ************************************************************************************
* File:    cpu.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## CPU system functions - from from lesson 2

The default CPU clock, for the AVR microcontroller, has prescaler of 6.
When calculating timer rates, either use (F_CPU/6) or disable the clock prescaler.

As a general rule, it is easier to disable the prescaler.

* ************************************************************************************/

#ifndef __CPU_H
#define __CPU_H

#define CLK_COMPENSATION(n) ((((uint32_t)(n)) * (1024 + ((int8_t)(SIGROW_OSC20ERR3V)))) / 1024)

void cpu_disable_clock_prescaler(void) {
	// Disable the CLK_PER prescaler
	CPU_CCP = CCP_IOREG_gc;					 // unlock protection (this is time critical)
	CLKCTRL.MCLKCTRLB = 0 << CLKCTRL_PEN_bp; // clear prescaler enable bit

}
#endif // __CPU_H
