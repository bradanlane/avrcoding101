/* ************************************************************************************
 * File:    tone.h
 * Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## Tone - timer generated simple square wave tones from lesson 4

Control an I/O pin from a timer without using an interrupt handler.

* ************************************************************************************/

#ifndef __TONE_H
#define __TONE_H

// an interval represents the whole wave
// we will be HIGH for half the interval and LOW for half the interval
// hence (n*2-1); then divide the CPU speed

#define FREQUENCY_TO_INTERVAL(n) (((F_CPU) / (n * 2)) - 1) // 16bit values

#define MINIMUM_FREQUENCY (((F_CPU) / 2) / 65535)

// the following definitions are provided for reference
#define FREQ_C4	 261.63
#define FREQ_C4S 277.18
#define FREQ_D4	 293.66
#define FREQ_D4S 311.13
#define FREQ_E4	 329.63
#define FREQ_F4	 349.23
#define FREQ_F4S 369.99
#define FREQ_G4	 392.00
#define FREQ_G4S 415.30
#define FREQ_A4	 440.00
#define FREQ_A4S 466.16
#define FREQ_B4	 493.88


void tone_off() {
	TCA0.SINGLE.CTRLA &= ~(TCA_SINGLE_ENABLE_bm); // stop timer
	PORTB.OUTCLR = PIN4_bm;
	PORTB.DIRCLR = PIN4_bm;
}


void tone_on(uint16_t frequency) {
	if (frequency == 0) {
		return;
		tone_off();
	}

	// some sane lower limit
	if (frequency < 11)
		frequency = 11;

	uint8_t prescaler = 0;
	// we chose any value greater than
	while (frequency < MINIMUM_FREQUENCY) {
		prescaler++;
		frequency = frequency * 2;
	}

	uint16_t interval = FREQUENCY_TO_INTERVAL(frequency);
	interval = CLK_COMPENSATION(interval);

	TCA0.SINGLE.CMP0 = interval;

	PORTB.DIRSET = PIN4_bm;

	// the prescaler are bits 3,2,1 of the CTRLA register
	TCA0.SINGLE.CTRLA = (prescaler << 1) | TCA_SINGLE_ENABLE_bm; /* start timer */
}


// set everything to a known starting point
void tone_setup(void) {
	/* switch waveform 1 output from PB1 to PB4 */
	PORTMUX.CTRLC = PORTMUX_TCA01_ALTERNATE_gc;

#if 1	// uses Frequency mode
	TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP1EN_bm | TCA_SINGLE_WGMODE_FRQ_gc;
#else 	// uses Period mode
	// WO0, WO1, and WO2, are controlled CMP0, CMP1, and CMP2
	TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP1EN_bm | TCA_SINGLE_WGMODE_DSBOTTOM_gc;
#endif

	// set the piezo buzzer pin to input
	// set the piezo buzzer pin to output LOW
	PORTB.DIRCLR = PIN4_bm;
	PORTB.OUTCLR = PIN4_bm;
}

#endif // __TONE_H
