/* ************************************************************************************
* File:    lesson3.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## lesson3.h - charlieplexed LEDs lesson for the AVR Coding 101 Class

Use inline code to control 42 LEDs from a simple 1 millisecond timer.

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/


// FYI: not all of these includes are needed for every lesson
// they are all included here to simplify progress through all the lessons

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>




// this definition makes the code easier to read
#define ALL_LED_PINS (PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm)

// these definitions are used to iterate through all of the LEDs
#define COL_COUNT 6
#define ROW_COUNT 7

// arrays represent the LEDs from left to right and top to bottom

const uint8_t _led_anodes[COL_COUNT * ROW_COUNT] = {
	PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN5_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN6_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN7_bm,
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm};
const uint8_t _led_cathodes[ROW_COUNT] = {
	PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm};

uint8_t _led_bitmap[ROW_COUNT]; // a unit8_t supports up to 8 LEDs in a single row

void led_on(uint8_t num) {
	num = num % (COL_COUNT * ROW_COUNT);
	uint8_t col = num % COL_COUNT;
	uint8_t row = num / COL_COUNT;
	_led_bitmap[row] |= (1 << col);
}

// set the bit in the display buffer to 0 so the LED will off when refreshed
void led_off(uint8_t num) {
	num = num % (COL_COUNT * ROW_COUNT);
	uint8_t col = num % COL_COUNT;
	uint8_t row = num / COL_COUNT;
	_led_bitmap[row] &= ~(1 << col);
}


void leds_bitmap_fill(void) {
	// when we are doing our setup, we can populate or clear our bitmap
	memset(_led_bitmap, 0xFF, ROW_COUNT); // a '1' bit sets the LED on
}

void leds_bitmap_clear(void) {
	// when we are doing our setup, we can populate or clear our bitmap
	memset(_led_bitmap, 0x00, ROW_COUNT); // a '1' bit sets the LED on
}

void leds_bitmap_load(const uint8_t *bitmap) {
	// warning: no error handling
	memcpy(_led_bitmap, bitmap, ROW_COUNT);
}


volatile uint8_t _led_active_row;

#define LED_INLINE_CODE	// this lets the milliseconds.h code know to include the update code

// fast row-at-a-time solution
static inline void leds_update(uint32_t timer) {
	if ((timer % 2) == 0) {
		PORTC.OUTSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header

		// we use one global variable: led_row
		uint8_t pins = 0;
		for (uint8_t col = 0; col < COL_COUNT; col++) {
			// we use bitwise bit shifting to test each bit of the bitmap
			if (_led_bitmap[_led_active_row] & (1 << col)) {
				// if the bit is set the we add the anode pin to our collection
				pins |= _led_anodes[(_led_active_row * COL_COUNT) + col];
			}
		}
		// reset all of the LED pins to input
		PORTA.OUTCLR = ALL_LED_PINS;
		PORTA.DIRCLR = ALL_LED_PINS;

		PORTA.DIRSET = (pins | _led_cathodes[_led_active_row]);
		// make the anodes as HIGH
		PORTA.OUTSET = pins;
		_led_active_row = (_led_active_row + 1) % ROW_COUNT;

		PORTC.OUTCLR = PIN2_bm;	// for debugging we can use PC2 from the breakout header
	}
}


// set everything to a known starting point
void leds_setup(void) {
	_led_active_row = 0;
	leds_bitmap_clear(); // all LEDs off to start
	PORTC.DIRSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header
}



#define TCA_INTERVAL (((F_CPU) / 1000) - 1)

volatile uint32_t _timer_counter = 0; // global variable holds the millisecond value

ISR(TCB0_INT_vect) {
	PORTC.OUTTGL = PIN2_bm;	// for debugging we can use PC2 from the breakout header

	// perform 1KHz aka milliseconds tasks
	_timer_counter++;
	leds_update(_timer_counter);
	TCB0.INTFLAGS = TCB_CAPT_bm; // must clear the interrupt flag
}

void milliseconds_setup() {
	uint16_t interval = TCA_INTERVAL;

	TCB0.CCMP = interval;			 // set the compare/capture register to our desired value (interval - 1)
	TCB0.INTCTRL = TCB_CAPT_bm;		 // enable the capture interrupt
	TCB0.CTRLB = TCB_CNTMODE_INT_gc; // timer mode: periodic interrupt; this is actually the default
	TCB0.CTRLA = TCB_ENABLE_bm;
	sei(); // enable interrupts

	PORTC.DIRSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header
}

uint32_t milliseconds(void) {
	uint32_t t;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { t = _timer_counter; }
	return t;
}

void milliseconds_delay(uint16_t ms) {
	uint32_t timer = milliseconds() + ms;
	while (timer > milliseconds()) {
		// spin wheels
	}
}


void cpu_disable_clock_prescaler(void) {
	// Disable the CLK_PER prescaler
	CPU_CCP = CCP_IOREG_gc;					 // unlock protection (this is time critical)
	CLKCTRL.MCLKCTRLB = 0 << CLKCTRL_PEN_bp; // clear prescaler enable bit

}

int main (void) {
	cpu_disable_clock_prescaler();
	leds_setup();
	milliseconds_setup();

	leds_bitmap_fill();

	uint32_t counter = 0;
	uint8_t off_num = 0;
	uint8_t on_num = 21;
	while (1) {
		#if 0
		// the interrupt service routing is refreshing the LEDs
		if (counter < milliseconds()) {
			led_on(on_num);
			led_off(off_num);
			on_num = (on_num + 1) % (ROW_COUNT * COL_COUNT);
			off_num = (off_num + 1) % (ROW_COUNT * COL_COUNT);
			// advance our test
			counter = milliseconds() + 50;
		}
		#endif
	}

	return 0;
}
