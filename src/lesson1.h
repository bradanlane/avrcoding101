/* ************************************************************************************
* File:    lesson1.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## lesson1.h - General I/O Pins lesson for the AVR Coding 101 Class

Control a single LED using a button. It demonstrates basic input and output using I/O pins.

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/

// FYI: not all of these includes are needed for every lesson
// they are all included here to simplify progress through all the lessons

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>




void led_setup(void) {
	// set both the cathode and anode pins of the first LED as output
    PORTA.DIRSET = PIN1_bm | PIN2_bm;
}

void led_on(void) {
	PORTA.OUTSET = PIN2_bm;
}

void led_off(void) {
	PORTA.OUTCLR = PIN2_bm;
}

void led_toggle(void) {
	PORTA.OUTTGL = PIN2_bm;
}



void button_setup(void) {
	// first (top) button
	PORTB.DIRCLR = PIN5_bm;			   // set button pin as input
	PORTB.PIN5CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin

	// second (bottom) button
	PORTC.DIRCLR = PIN3_bm;			   // set button pin as input
	PORTC.PIN3CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin
}

bool button1_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTB.IN & (PIN5_bm)) == 0)
		return true;
	return false;
}

bool button2_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTC.IN & (PIN3_bm)) == 0)
		return true;
	return false;
}



int main (void) {
	button_setup();
	led_setup();

	while (1) {
        if (button1_pressed()) {
			led_on();
		}
		else {
			led_off();
		}
	}
}
