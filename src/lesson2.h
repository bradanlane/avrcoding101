/* ************************************************************************************
* File:    lesson2.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## lesson2.h - Basic Timer lesson for the AVR Coding 101 Class

Create a simple 1 millisecond timer and use the result to control a single LED using a button.

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/

// FYI: not all of these includes are needed for every lesson
// they are all included here to simplify progress through all the lessons

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


void led_setup(void) {
	// set both the cathode and anode pins of the first LED as output
    PORTA.DIRSET = PIN1_bm | PIN2_bm;
}

void led_on(void) {
	PORTA.OUTSET = PIN2_bm;
}

void led_off(void) {
	PORTA.OUTCLR = PIN2_bm;
}

void led_toggle(void) {
	PORTA.OUTTGL = PIN2_bm;
}



void button_setup(void) {
	// first (top) button
	PORTB.DIRCLR = PIN5_bm;			   // set button pin as input
	PORTB.PIN5CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin

	// second (bottom) button
	PORTC.DIRCLR = PIN3_bm;			   // set button pin as input
	PORTC.PIN3CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin
}

bool button1_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTB.IN & (PIN5_bm)) == 0)
		return true;
	return false;
}

bool button2_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTC.IN & (PIN3_bm)) == 0)
		return true;
	return false;
}




volatile uint32_t _timer_counter = 0; // global variable holds the millisecond value

#define TCA_INTERVAL (((F_CPU) / 1000) - 1)

ISR(TCB0_INT_vect) {
	_timer_counter++;
	PORTC.OUTTGL = PIN2_bm;		// for debugging we toggle a spare pin every interrupt
	TCB0.INTFLAGS = TCB_CAPT_bm; // must clear the interrupt flag
}

void milliseconds_setup() {
	uint16_t interval = TCA_INTERVAL;
	//interval = CLK_COMPENSATION(interval);

	TCB0.CCMP = interval;		// set the compare/capture register to our desired value (interval - 1)
	TCB0.INTCTRL = TCB_CAPT_bm; // enable the capture interrupt
	TCB0.CTRLA = TCB_ENABLE_bm;

	PORTC.DIRSET = PIN2_bm;		// for debugging we set a spare pin as output
	sei(); // enable interrupts
}

uint32_t milliseconds(void) {
    uint32_t t;

	// an 'atomic block' will execute in its entirety without interruption
	// the `ATOMIC_RESTORESTATE` insures the system is as it was before the block

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        t = _timer_counter;
    }

    return t;
}



void cpu_disable_clock_prescaler(void) {
	// Disable the CLK_PER prescaler
	CPU_CCP = CCP_IOREG_gc;					// unlock protection (this is time critical)
	CLKCTRL.MCLKCTRLB &= ~(CLKCTRL_PEN_bm);	// clear prescaler enable bit
}


int main (void) {
	cpu_disable_clock_prescaler();
	milliseconds_setup();
	led_setup();

	uint32_t counter = milliseconds();

	while (1) {
		if (counter < milliseconds()) {
			led_toggle();
			// advance our test 1000 milliseconds
			counter += 1000;
		}
	}

	return 0;
}
