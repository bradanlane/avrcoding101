/* ************************************************************************************
 * File:    lesson4.h
 * Date:    2023.01.16
 * Author:  Bradán Lane STUDIO
 *
 * This content may be redistributed and/or modified as outlined under the MIT License
 *
 * ************************************************************************************

## lesson4.h - Timer modulation of I/O lesson for the AVR Coding 101 Class

Control an I/O pin from a timer without using an interrupt handler.
The button example from lesson 1 is used to turn on/off the timer.

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/


// FYI: not all of these includes are needed for every lesson
// they are all included here to simplify progress through all the lessons

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


#define CLK_COMPENSATION(n) ((((uint32_t)(n)) * (1024 + ((int8_t)(SIGROW_OSC20ERR3V)))) / 1024)

void cpu_disable_clock_prescaler(void) {
	// Disable the CLK_PER prescaler
	CPU_CCP = CCP_IOREG_gc;					 // unlock protection (this is time critical)
	CLKCTRL.MCLKCTRLB = 0 << CLKCTRL_PEN_bp; // clear prescaler enable bit

}



#define TCA_INTERVAL (((F_CPU) / 1000) - 1)

volatile uint32_t _timer_counter = 0; // global variable holds the millisecond value

ISR(TCB0_INT_vect) {
	// PORTC.OUTTGL = PIN2_bm;	// for debugging we can use PC2 from the breakout header

	// perform 1KHz aka milliseconds tasks
	_timer_counter++;
#ifdef LED_INLINE_CODE // include the LEDs update code
	leds_update(_timer_counter);
#endif
	TCB0.INTFLAGS = TCB_CAPT_bm; // must clear the interrupt flag
}

void milliseconds_setup() {
	uint16_t interval = TCA_INTERVAL;
	interval = CLK_COMPENSATION(interval);

	TCB0.CCMP = interval;			 // set the compare/capture register to our desired value (interval - 1)
	TCB0.INTCTRL = TCB_CAPT_bm;		 // enable the capture interrupt
	TCB0.CTRLB = TCB_CNTMODE_INT_gc; // timer mode: periodic interrupt; this is actually the default
	TCB0.CTRLA = TCB_ENABLE_bm;
	sei(); // enable interrupts

	//PORTC.DIRSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header
}

uint32_t milliseconds(void) {
	uint32_t t;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { t = _timer_counter; }
	return t;
}

void milliseconds_delay(uint16_t ms) {
	uint32_t timer = milliseconds() + ms;
	while (timer > milliseconds()) {
		// spin wheels
	}
}



void buttons_setup(void) {
	// first (top) button
	PORTB.DIRCLR = PIN5_bm;			   // set button pin as input
	PORTB.PIN5CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin

	// second (bottom) button
	PORTC.DIRCLR = PIN3_bm;			   // set button pin as input
	PORTC.PIN3CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin
}

bool button1_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTB.IN & (PIN5_bm)) == 0)
		return true;
	return false;
}

bool button2_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTC.IN & (PIN3_bm)) == 0)
		return true;
	return false;
}



// an interval represents the whole wave
// we will be HIGH for half the interval and LOW for half the interval
// hence (n*2-1); then divide the CPU speed

#define FREQUENCY_TO_INTERVAL(n) (((F_CPU) / (n * 2)) - 1) // 16bit values

#define MINIMUM_FREQUENCY (((F_CPU) / 2) / 65535)

// the following definitions are provided for reference
#define FREQ_C4	 261.63
#define FREQ_C4S 277.18
#define FREQ_D4	 293.66
#define FREQ_D4S 311.13
#define FREQ_E4	 329.63
#define FREQ_F4	 349.23
#define FREQ_F4S 369.99
#define FREQ_G4	 392.00
#define FREQ_G4S 415.30
#define FREQ_A4	 440.00
#define FREQ_A4S 466.16
#define FREQ_B4	 493.88


void tone_off() {
	TCA0.SINGLE.CTRLA &= ~(TCA_SINGLE_ENABLE_bm); // stop timer
	PORTB.OUTCLR = PIN4_bm;
	PORTB.DIRCLR = PIN4_bm;
}


void tone_on(uint16_t frequency) {
	if (frequency == 0) {
		tone_off();
		return;
	}

	uint8_t prescaler = 0;

#if 1
	// check our frequency is greater than the minimum
	while (frequency < MINIMUM_FREQUENCY) {
		// the prescaler increments in powers of 2
		prescaler++;
		frequency = frequency * 2;
	}
#endif

	uint16_t interval = FREQUENCY_TO_INTERVAL(frequency);
	//interval = CLK_COMPENSATION(interval);

	TCA0.SINGLE.CMP0 = interval;

	PORTB.DIRSET = PIN4_bm;

	// the prescaler are bits 3,2,1 of the CTRLA register
	TCA0.SINGLE.CTRLA = (prescaler << 1) | TCA_SINGLE_ENABLE_bm; /* start timer */
}


// set everything to a known starting point
void tone_setup(void) {
	/* switch waveform 1 output from PB1 to PB4 */
	PORTMUX.CTRLC = PORTMUX_TCA01_ALTERNATE_gc;

	// set the piezo buzzer pin to input
	// set the piezo buzzer pin to output LOW
	PORTB.DIRSET = PIN4_bm;
	PORTB.OUTCLR = PIN4_bm;

	// uses Frequency mode
	TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP1EN_bm | TCA_SINGLE_WGMODE_FRQ_gc;
}



int main(void) {
	cpu_disable_clock_prescaler();
	tone_setup();
	buttons_setup();

	bool pressed = false;

	while (1) {
		if (button1_pressed()) {
			if (!pressed) {
				pressed = true;
				tone_on(200);
			}
		}
		else if (button2_pressed()) {
			if (!pressed) {
				pressed = true;
				tone_on(100);
			}
		}
		else {
			// 1 means the button is released
			if (pressed) {
				pressed = false;
				tone_off();
			}
		}
	}
}
