/* ************************************************************************************
* File:    lesson6.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## lesson6.h - EEPROM and Signature bytes lesson for the AVR Coding 101 Class

Write and read a byte from EEPROM
Read the unique signature bytes for the microcontroller

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/

// FYI: not all of these includes are needed for every lesson
// they are all included here to simplify progress through all the lessons

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>




#define CLK_COMPENSATION(n) ((((uint32_t)(n)) * (1024 + ((int8_t)(SIGROW_OSC20ERR3V)))) / 1024)

void cpu_disable_clock_prescaler(void) {
	// Disable the CLK_PER prescaler
	CPU_CCP = CCP_IOREG_gc;					 // unlock protection (this is time critical)
	CLKCTRL.MCLKCTRLB = 0 << CLKCTRL_PEN_bp; // clear prescaler enable bit

}



#define TCA_INTERVAL (((F_CPU) / 1000) - 1)

volatile uint32_t _timer_counter = 0; // global variable holds the millisecond value

ISR(TCB0_INT_vect) {
	// PORTC.OUTTGL = PIN2_bm;	// for debugging we can use PC2 from the breakout header

	// perform 1KHz aka milliseconds tasks
	_timer_counter++;
#ifdef LED_INLINE_CODE // include the LEDs update code
	leds_update(_timer_counter);
#endif
	TCB0.INTFLAGS = TCB_CAPT_bm; // must clear the interrupt flag
}

void milliseconds_setup() {
	uint16_t interval = TCA_INTERVAL;
	interval = CLK_COMPENSATION(interval);

	TCB0.CCMP = interval;			 // set the compare/capture register to our desired value (interval - 1)
	TCB0.INTCTRL = TCB_CAPT_bm;		 // enable the capture interrupt
	TCB0.CTRLB = TCB_CNTMODE_INT_gc; // timer mode: periodic interrupt; this is actually the default
	TCB0.CTRLA = TCB_ENABLE_bm;
	sei(); // enable interrupts

	//PORTC.DIRSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header
}

uint32_t milliseconds(void) {
	uint32_t t;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { t = _timer_counter; }
	return t;
}

void milliseconds_delay(uint16_t ms) {
	uint32_t timer = milliseconds() + ms;
	while (timer > milliseconds()) {
		// spin wheels
	}
}



void buttons_setup(void) {
	// first (top) button
	PORTB.DIRCLR = PIN5_bm;			   // set button pin as input
	PORTB.PIN5CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin

	// second (bottom) button
	PORTC.DIRCLR = PIN3_bm;			   // set button pin as input
	PORTC.PIN3CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin
}

bool button1_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTB.IN & (PIN5_bm)) == 0)
		return true;
	return false;
}

bool button2_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTC.IN & (PIN3_bm)) == 0)
		return true;
	return false;
}



// this formula appears different from the one published by Microchip(r)
// it avoids floating point math while generating the same result
#define BAUD_RATE_FORMULA(b) ((((8 * F_CPU) / b) + 1) / 2)

void usart_setup(uint32_t baudrate) {
	// cli(); // disable all interrupt handlers

	// set  baud rate
	USART0.BAUD = (uint16_t)BAUD_RATE_FORMULA(baudrate);

	// the default USART pins are 2 and 3 on PORTB
	PORTB.DIRSET = PIN2_bm; // TX pin is output
	PORTB.DIRCLR = PIN3_bm; // RX pin is input

	USART0.CTRLB |= USART_TXEN_bm; // enable TX
}

/* ---
transmit a byte
-- */
int usart_put_byte(uint8_t data) {
	// wait for any previous TX to be finished
	while (!(USART0.STATUS & USART_DREIF_bm)) {
		;
	}
	// put new byte into the TX data register
	USART0.TXDATAL = data;
	return data;
}

/* ---
transmit a string one byte at a time
-- */
void usart_put_string(const char *text) {
	// no error handling
	for (int i = 0; i < strlen(text); i++)
		usart_put_byte((uint8_t)text[i]);
}




int main (void) {
	cpu_disable_clock_prescaler();
	buttons_setup();
	usart_setup(115200);

	uint8_t byte, half;
	const uint8_t* serno = &(SIGROW_SERNUM0);

	bool pressed = false;

	while (1) {
		if (button1_pressed()) {
			if (!pressed) {
				pressed = true;

				// read and output the serial number
				for (int i = 0; i < 10; i++) {
					byte = serno[i];

					half = (byte >> 4) & 0x0F;
					if (half > 9)	half = 'A' + (half - 10);
					else			half = '0' + (half);
					usart_put_byte(half);

					half = (byte) & 0x0F;
					if (half > 9)	half = 'A' + (half - 10);
					else			half = '0' + (half);
					usart_put_byte(half);

					usart_put_byte(' ');
				}
				usart_put_byte('\n');
			}
		} else {
			if (pressed) {
				pressed = false;
			}
		}
	}

	return 0;
}
