/* ************************************************************************************
* File:    lesson6.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## lesson6.h - EEPROM and Signature bytes lesson for the AVR Coding 101 Class

Write and read a byte from EEPROM
Read the unique signature bytes for the microcontroller

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/

// FYI: not all of these includes are needed for every lesson
// they are all included here to simplify progress through all the lessons

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>




#define CLK_COMPENSATION(n) ((((uint32_t)(n)) * (1024 + ((int8_t)(SIGROW_OSC20ERR3V)))) / 1024)

void cpu_disable_clock_prescaler(void) {
	// Disable the CLK_PER prescaler
	CPU_CCP = CCP_IOREG_gc;					 // unlock protection (this is time critical)
	CLKCTRL.MCLKCTRLB = 0 << CLKCTRL_PEN_bp; // clear prescaler enable bit

}



#define TCA_INTERVAL (((F_CPU) / 1000) - 1)

volatile uint32_t _timer_counter = 0; // global variable holds the millisecond value

ISR(TCB0_INT_vect) {
	// PORTC.OUTTGL = PIN2_bm;	// for debugging we can use PC2 from the breakout header

	// perform 1KHz aka milliseconds tasks
	_timer_counter++;
#ifdef LED_INLINE_CODE // include the LEDs update code
	leds_update(_timer_counter);
#endif
	TCB0.INTFLAGS = TCB_CAPT_bm; // must clear the interrupt flag
}

void milliseconds_setup() {
	uint16_t interval = TCA_INTERVAL;
	interval = CLK_COMPENSATION(interval);

	TCB0.CCMP = interval;			 // set the compare/capture register to our desired value (interval - 1)
	TCB0.INTCTRL = TCB_CAPT_bm;		 // enable the capture interrupt
	TCB0.CTRLB = TCB_CNTMODE_INT_gc; // timer mode: periodic interrupt; this is actually the default
	TCB0.CTRLA = TCB_ENABLE_bm;
	sei(); // enable interrupts

	//PORTC.DIRSET = PIN2_bm;	// for debugging we can use PC2 from the breakout header
}

uint32_t milliseconds(void) {
	uint32_t t;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) { t = _timer_counter; }
	return t;
}

void milliseconds_delay(uint16_t ms) {
	uint32_t timer = milliseconds() + ms;
	while (timer > milliseconds()) {
		// spin wheels
	}
}



void buttons_setup(void) {
	// first (top) button
	PORTB.DIRCLR = PIN5_bm;			   // set button pin as input
	PORTB.PIN5CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin

	// second (bottom) button
	PORTC.DIRCLR = PIN3_bm;			   // set button pin as input
	PORTC.PIN3CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin
}

bool button1_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTB.IN & (PIN5_bm)) == 0)
		return true;
	return false;
}

bool button2_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTC.IN & (PIN3_bm)) == 0)
		return true;
	return false;
}



// the following are defined in the microcontroller header file: EEPROM_START and EEPROM_SIZE
#define EEPROM_MAX (EEPROM_START + EEPROM_SIZE)


uint8_t eeprom_get_byte(uint16_t relative_addr) {
	uint8_t b = 0;

	if (relative_addr < EEPROM_SIZE) {
		// Read operation will be stalled by hardware if any write is in progress
		b = *(uint8_t *)(EEPROM_START + relative_addr);
	}

	return b;
}


void eeprom_put_byte(uint16_t relative_addr, uint8_t data) {
	if (relative_addr < EEPROM_SIZE) {

		/* Wait for completion of any previous write */
		while (NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm)
			;

		// Load in the relevant EEPROM page by writing directly to the memory address
		*(uint8_t *)(EEPROM_START + relative_addr) = data;

		// Unlock self programming and then erase/write the 'page' (one byte)
		CCP = CCP_SPM_gc;
		NVMCTRL.CTRLA = NVMCTRL_CMD_PAGEERASEWRITE_gc;
	}
}


int main (void) {
	cpu_disable_clock_prescaler();
	buttons_setup();

	bool pressed = false;
	uint8_t value = eeprom_get_byte(1);

	while (1) {
		if (button1_pressed()) {
			if (!pressed) {
				pressed = true;
				value++;
				eeprom_put_byte(1, value);
			}
		} else {
			if (pressed) {
				pressed = false;
			}
		}
	}

	return 0;
}
