/* ************************************************************************************
* File:    lesson5.h
* Date:    2023.01.16
* Author:  Bradán Lane STUDIO
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************

## lesson5.h - Serial (USART) I/O lesson for the AVR Coding 101 Class

Use the USART interrupt handlers to send data and to receive data.
A simple test is to uppercase lowercase letters and lowercase uppercase letters.
Send a short message when a button is pressed.

NOTE: to simplify the lesson, usart_buffers.h provides a pair of circular buffers
to handle RX and TX data.

Each lesson consists of the lesson documentation and coding exercise(s).
The culmination of all exercises for a given lesson are provided in a "lesson<n>.h" file
with the same number as the lesson.

* ************************************************************************************/

// FYI: not all of these includes are needed for every lesson
// they are all included here to simplify progress through all the lessons

#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


#define CLK_COMPENSATION(n) ((((uint32_t)(n)) * (1024 + ((int8_t)(SIGROW_OSC20ERR3V)))) / 1024)

void cpu_disable_clock_prescaler(void) {
	// Disable the CLK_PER prescaler
	CPU_CCP = CCP_IOREG_gc;					 // unlock protection (this is time critical)
	CLKCTRL.MCLKCTRLB = 0 << CLKCTRL_PEN_bp; // clear prescaler enable bit

}


void buttons_setup(void) {
	// first (top) button
	PORTB.DIRCLR = PIN5_bm;			   // set button pin as input
	PORTB.PIN5CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin

	// second (bottom) button
	PORTC.DIRCLR = PIN3_bm;			   // set button pin as input
	PORTC.PIN3CTRL = PORT_PULLUPEN_bm; // enable the pull-up on the button pin
}

bool button1_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTB.IN & (PIN5_bm)) == 0)
		return true;
	return false;
}

bool button2_pressed (void) {
	// read the button pin to see if it is 0 or 1
	// 0 means the button is pressed
	if ((PORTC.IN & (PIN3_bm)) == 0)
		return true;
	return false;
}


// this formula appears different from the one published by Microchip(r)
// it avoids floating point math while generating the same result
#define BAUD_RATE_FORMULA(b) ((((8 * F_CPU) / b) + 1) / 2)

void usart_setup(uint32_t baudrate) {
	// the default USART pins are 2 and 3 on PORTB
	PORTB.DIRSET = PIN2_bm; // TX pin is output
	PORTB.DIRCLR = PIN3_bm; // RX pin is input

	// set  baud rate
	uint16_t interval = (uint16_t)BAUD_RATE_FORMULA(baudrate);
	//interval = CLK_COMPENSATION(interval);

	USART0.BAUD = interval;

	USART0.CTRLB |= USART_TXEN_bm; // enable TX
}

/* ---
transmit a byte
-- */
int usart_put_byte(uint8_t data) {
	// wait for any previous TX to be finished
	while (!(USART0.STATUS & USART_DREIF_bm)) {
		;
	}
	// put new byte into the TX data register
	USART0.TXDATAL = data;
	return data;
}

/* ---
transmit a string one byte at a time
-- */
int usart_put_string(const char *text) {
	// no error handling
	uint16_t len = strlen(text);
	for (int i = 0; i < len; i++)
		usart_put_byte((uint8_t)text[i]);
	return len;
}

int main(void) {
	cpu_disable_clock_prescaler();
	buttons_setup();
	usart_setup(115200);

	bool message = false;

	while (1) {
		if (button1_pressed()) {
			if (!message) {
				message = true;
				usart_put_string("hello world\n");
			}
		} else {
			if (message) {
				message = false;
			}
		}
	}

	return 0;
}
