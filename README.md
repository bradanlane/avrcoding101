# AVR Coding 101

The repository is the class material for **AVR Coding 101**
Welcome to the class material for **AVR Coding 101**
and the companion hardware, the **ACK1** _(AVR Coding Kit)_, [available from Tindie](https://www.tindie.com/products/bradanlane/ack1-avr-coding-kit/).
The contents of this repository, the lesson materials, and the code are published under the [MIT license](https://choosealicense.com/licenses/mit/).

<div align="center">![AVR Coding Kit ACK1](files/ACK1_diagram.png)</div>

the **ACK1** uses the ATtiny1616 microcontroller. This chip is from tinyAVR® 1-series of microcontrollers. It has 16KB Flash, 2KB of SRAM, and 256bytes of EEPROM in a 20-pin package. It has  17 available I/O pins (the **ACK1** uses 12). The microcontroller will run at up to 20 MHz.

The **AVR Coding 101** lessons cover:

- The **ACK1** hardware kit
  - the ATtiny1616 features
  - the CH340E USB-UART adapter
- AVR coding syntax
- Turning on/off an LEDs attached to I/O pins
- Reading a button from an I/O pin
- Creating a timer (TCB0) and updating a variable in an interrupt service routine (ISR)
- Controlling charlieplexed LEDs
  - Using a timer (TCB0) to refresh a matrix of LEDs
  - Creating a simple display buffer
  - Calculating refresh rates
- Creating a timer (TBA0) to control an I/O pin to make a tone
- Implementing Serial I/O using the USART interrupt service routines
- Storing data in EEPROM
- Reading the microcontroller signature
- Reading and updating the microcontroller configuration FUSES

For details, please read the [syllabus](documentation/syllabus.md).

\* **AVR Coding 101** lessons may be completed using your own hardware and have examples specific to the **ACK1** board, [available from Tindie](https://www.tindie.com/products/bradanlane/ack1-avr-coding-kit/).