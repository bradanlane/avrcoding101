# AVR Coding 101: Lesson 3

<table border="0"><tr><td width="48%">
The goal of lesson 3 is use the AVR microcontroller timer TCB0 to control charlieplexed LEDs.
By the end of this lesson, you should have accomplished the following:

- Understand charlieplexed LEDs
- Setup the data needed to manage charlieplexed LEDs
- Calculate timer intervals for persistence of vision
- Modify the _Interrupt Service Routine_ from lesson 2 to update LEDs
- Using a bitmap
  - for graphics
  - for font characters
  - Scrolling text
</td><td width="48%" align="center">
<a href="https://youtu.be/hpjDD9maifs" target="_blank"><img src="files/avrcoding101_lesson3.jpg" alt="AVR Coding 101 - Lesson 3"></a>
<br/>
<i>Companion Video of Lesson 3<br> (click to watch)</i>
<br/><br/>
</td></tr></table>

[[_TOC_]]

## Charlieplexing LEDs

Lesson 1 explained an LED has an anode and a cathode.
The LED will light when a voltage is applied to the anode while the cathode is connected to a GND source.
If you connect an LED backwards, meaning you connect it's anode to GND and it's cathode to a voltage source, it will not light.

_(If you increase the voltage enough, you will eventually release the magic smoke from the LED and it will die.)_

The simplest microcontroller circuit would be to connect the anode to an I/O pin and connect the cathode to the same GND as used by the microcontroller.
This configuration supports one LED for each available pin.

The **ACK1** used 7 I/O pins for 42 LEDs. How?!

The **ACK1** connects each LED between 2 pins and does not use the physical GND used by the microcontroller. Why?!

The answer to both os these questions is _Charlieplexing_.

If we connect two LEDs to the same two I/O pins but one is connected anode-cathode, while the seconds is cathode-anode,
then we can light either by coding one pin HIGH and the other pin LOW and switching which pin is HIGH.

<div align="center">![Charlieplexing Example with 3 Pins](files/charlieplexing.gif)<br/>
<i>animation courtesy of Brian Lough<br/>[YouTube Video](https://youtu.be/b44VGTaCSk8)</i><br/><br/>
</div>

With charlieplexing, two pins will support two LEDs.
This is not better than simple circuit.
However, when we use three pins, we have six combinations we can use for the LEDs.
The formula is _(N \* (N -1))_ where _N_ is the number of pins used for LEDs.
The **ACK1** uses 7 pins which is (7 \* (7 - 1))  = (7 \* 6) = 42 LEDs.

However, there is a problem with charlieplexed LEDs.
You can only reliably light LEDs which do not share the same cathode or anode because some combinations of HIGH and LOW pins will light more LEDs than you might want.

The solution is to turn LEDs on and off very quickly and use _persistence of vision_ so it looks like multiple LEDs are on at the same time.

A timer is ideal for performing a task very quickly over and over!

### Light 2 LEDs with Charlieplexing

Looking at the **ACK1**, all of the LEDs use PORTA.

- Each LED is connected to two pins
- The 2-digit numbers adjacent to the LEDs represent the cathode:anode for a given LED

To simplify describing these, we will combine the PORTx and PINn.
The top left LED is labeled `12` which refers to `PA1` and `PA2`.
The LED immediately to its right is `PA1` and `PA3`.

Here is a quick review, before starting the exercise, for setting I/O pins:

```C
    PORTA.DIRSET = PIN1_bm;         // PIN1 on PORTA is output

    PORTA.OUTSET = PIN1_bm;         // PIN1 on PORTA is HIGH
    PORTA.OUTCLR = PIN1_bm;         // PIN1 on PORTA is LOW

    PORTA.DIRCLR = PIN1_bm;         // PIN1 on PORTA is input
```

**Exercise:** Edit `main.c` to configure the interrupt service routine to switch between the first two LED being on/off once each second.

You need to write the necessary code to set the anode and cathode of one LED as HIGH and LOW respectively while the other LED of off. Then do the opposite.

### Light all the LEDs with Charlieplexing

To manage the anodes and cathodes or all the LEDs,
it could be written out with individual PINn code but that will be a lot of code
and we want to keep the interrupt service routine short and fast.
A better solution is to maintain a small amount of data which represents the pins.

### Bitmap Charlieplexed LEDs

A bitmap is a map of bits (duh).
The bitmap we will create maintains the state of each LED.
If we are only interested in a binary state (on or off) then we only need one `bit`
to represent each LED.
If we had RGB LEDs or we wanted to support levels of brightness, then we would need
more bits.

There are two competing characteristics of a display bitmap - data size and access speed.

>The bitmap for the **ACK1** needs to support 6x7.
>The on/off data could be 6 bytes with 7 bits each or 7 bytes of 6 bits each.
>
>All of the LEDs in a row share a common pin. It's less code (and thus faster) to process the bitmap row-wise". However, if the goal is to support horizontal scrolling, then a column-wise bitmap is easier to code.
>
>This is an example where it's necessary to consider hardware design on software design when pursuing maximum optimization.

First, we maintain a list of all of the anode assignments and a list of the cathode assignments.

Each row of the **ACK1** has a common cathode.
We optimize the list of cathodes to only represent the rows.

```C
// these definitions are used to iterate through all of the LEDs
#define COL_COUNT 6
#define ROW_COUNT 7

// arrays represent the LEDs from left to right and top to bottom

const uint8_t _led_anodes[ROW_COUNT*COL_COUNT] = {
    PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
    PIN1_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
    PIN1_bm, PIN2_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm,
    PIN1_bm, PIN2_bm, PIN3_bm, PIN5_bm, PIN6_bm, PIN7_bm,
    PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN6_bm, PIN7_bm,
    PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN7_bm,
    PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm
};
const uint8_t _led_cathodes[ROW_COUNT] = {
    PIN1_bm, PIN2_bm, PIN3_bm, PIN4_bm, PIN5_bm, PIN6_bm, PIN7_bm
};

// this definition makes the code easier to read
#define ALL_LED_PINS (PIN1_bm | PIN2_bm | PIN3_bm | PIN4_bm | PIN5_bm | PIN6_bm | PIN7_bm)
```

We will iterate through all of the anodes and cathodes so we need a couple of variables to track the current LED.

```C
uint8_t _led_active_row;
uint8_t _led_active_col;
```

We need to track with LEDs are on and off. We want a _map_ of the LEDs.
The map could be 42 values (booleans or small integers).
But, if all we are interested in is if an LED is on or off then we only need a single bit.
A Row of LEDs then is 6 bits.

We only need one byte for each row. We also need to be able to keep track of which row to light.

```C
uint8_t _led_bitmap[ROW_COUNT];

void leds_bitmap_fill(void) {
    // when we are doing our setup, we can populate or clear our bitmap
    for (uint8_t row = 0; row < ROW_CONT; row++) {
        _led_bitmap[row] = 0b00111111; // set all LEDs in a row ON
    }
}

void leds_bitmap_clear(void) {
    // when we are doing our setup, we can populate or clear our bitmap
    for (uint8_t row = 0; row < ROW_CONT; row++) {
        _led_bitmap[row] = 0b00000000; // set all LEDs in a row OFF
    }
}
```

This lesson is already running long so here are two clues to completing the exercise.

First, when lighting one LED, setting the unused pins to input, prevents the connected LEDs from lighting.

Second, you can change lots of pins at the same time.

Setting all pins to input looks like this:

```C
static inline void leds_update(uint32_t timer) {
    // reset all of the LED pins to input
    PORTA.OUTCLR = ALL_LED_PINS;
    PORTA.DIRCLR = ALL_LED_PINS;
}
```

Now, all the LEDs are reset.
Check if the new LED needs to be lit and set it' anode and cathode correctly.

```C
static inline void leds_update(uint32_t timer) {
    // reset all of the LED pins to input
    PORTA.OUTCLR = ALL_LED_PINS;
    PORTA.DIRCLR = ALL_LED_PINS;

    // we use two variables: timer_row and timer_col to indicate which LED is active
    // make both pin as output
    if (_led_bitmap[_led_active_row] & (1 << _active_col)) {
        PORTA.DIRSET = (_led_anodes[(_led_active_row * ROW_COUNT) + _led_active_col] | _led_cathodes[_led_active_row]);
        // make the anode as HIGH
        PORTA.OUTSET = _led_anodes[(_led_active_row * ROW_COUNT) + _led_active_col];
        // increment col & row through all 42 LEDs
        _led_active_col = (_led_active_col + 1) % COL_COUNT;
        if (_led_active_col == 0)
            _led_active_row = (_led_active_row + 1) % ROW_COUNT;
    }
}
```

**IMPORTANT:** We want this code to run as quickly as possible.
Using the `static inline` directive, we are instructing the compiler
to include this new code directly into where it is referenced vs making a function call.
This means this code must be added before we include the milliseconds timer code.

**Exercise:** Edit `main.c` to configure the interrupt service routine to cycle across all of the LEDs, turning one on while the rest are off.

Your code should look something like this:

```C
#include "cpu.h"

#define LED_INLINE_CODE
// your inline LED code

#include "milliseconds.h"
#include "buttons.h"

// ... rest of your code ...

int main (void) {
    cpu_disable_clock_prescaler();
    milliseconds_setup();

    // ... rest of your code ...

    while (1) {
        // the interrupt service routing is controlling the LED
    }

    return 0;
}

// ... rest of your code
```

**Quiz:** How fast does the timer need to be to light every LED and not flicker?

<details>

<summary><b>Answer ...</b></summary>

The generally accepted repeat rate for _flicker fusion_ is 50-60Hz.
With 42 LEDs \* 50Hz the interval for a single LED is (50 \* 42) = 2100 times per second.

Our current `CCMP` for `TCB0` is 20,000 which results in 1000 times per second.
Do you perceive flicker?

</details>

### Bitmap Charlieplexed LEDs - Optimized

The current design processes one LED at a time.
It is possible to process all LEDs which share a common anode or a common cathode.

The LEDs on the **ACK1** are in rows based on the cathode.
We can process all the LEDs in a row at the same time.

In our inline code, we need to determine which LEDs in the row have their bit set.
These represent the anodes which need to be set as output.
We also need to know which row is being processed which represents the cathode.
We still reset all the LEDs as before.
The last step is to light any LEDs in the active row.

```C
static inline void leds_update(uint32_t timer) {
    if ((timer % 3) == 0) {
        // we use one global variable: _led_active_row

        uint8_t pins = 0;

        for (uint8_t col = 0; col < COL_COUNT; col++) {
            // we use bitwise bit shifting to test each bit of the bitmap
            if (_led_bitmap[_led_active_row] & (1 << col)) {
                // if the bit is set the we add the anode pin to our collection
                pins |= _led_anodes[(_led_active_row * COL_COUNT) + col];
            }
        }

        // reset all of the LED pins to input
        PORTA.OUTCLR = ALL_LED_PINS;
        PORTA.DIRCLR = ALL_LED_PINS;

        // make all the anodes and the one cathode as output
        PORTA.DIRSET = (pins | _led_cathodes[_led_active_row]);
        // make the anodes as HIGH
        PORTA.OUTSET = pins;
        // increment to the next row for the next update
        _led_active_row = (_led_active_row + 1) % ROW_COUNT;
    }
}
```

While we are cleaning up our code, let's add a few helper functions.

```C
// set everything to a known starting point
void leds_setup(void) {
    _led_active_row = 0;
    leds_bitmap_clear(); // all LEDs off to start
}

void led_on(uint8_t num) {
    num = num % (COL_COUNT * ROW_COUNT);
    uint8_t col = num % COL_COUNT;
    uint8_t row = num / COL_COUNT;
    _led_bitmap[row] |= (1 << col);
}

// set the bit in the display buffer to 0 so the LED will off when refreshed
void led_off(uint8_t num) {
    num = num % (COL_COUNT * ROW_COUNT);
    uint8_t col = num % COL_COUNT;
    uint8_t row = num / COL_COUNT;
    _led_bitmap[row] &= ~(1 << col);
}

// copy an entire set of LED assignments
void leds_bitmap_load(const uint8_t *bitmap) {
    // warning: no error handling
    memcpy(_led_bitmap, bitmap, ROW_COUNT);
}
```

**Exercise:** Edit `main.c` to configure the interrupt service routine to cycle across all rows of the LEDs, using a bitmap to turn on the active ones while the rest are off.

**Quiz:** How fast does the timer need to be to light every LED and not flicker?

<details>

<summary><b>Answer ...</b></summary>

The generally accepted repeat rate for _flicker fusion_ is 50-60Hz.
With 7 rows of LEDs \* 50Hz the interval for a single LED is (50 \* 7) = 350 times per second.

Our current `CCMP` for `TCB0` is 20,000 which results in 1000 times per second.
The example code above uses `(timer % 3)` which means we update the LEDs every 3 intervals or 333 times per second.
Do you perceive flicker?

If you change to  `(timer % 4)` do the LEDs look smooth?
If you change to  `(timer % 2)` do the LEDs look smooth?

_When you do the above experiments, take a look at the size of the compiled code. You may be surprised._
</details>

**Quiz:** How much time is spent within the timer interrupt service handler to update the a row of LEDS?

<details>

<summary><b>Answer ...</b></summary>

It is possible to look at the number of assembler instructions,
add up all of the execution cycles, and compute the time to complete
a single execution of `leds_update()` when `((timer % 3) == 0)`.

A much easier method is to measure the time from start to finish.
This is another use case for the oscilloscope.

The `led_update()` is call every millisecond but only does anything significant
ever other millisecond, or 500 times per second.
When `led_update()` does need to update the LEDs, the majority of the code is executed.
That code only takes 27.5 microseconds to complete _(with the F_CPU running at 20MHz)_.

<div align="center">![led_update() rate](files/oscilloscope_led_updatess.png){width=48%} ![led_update() duration](files/oscilloscope_led_update.png){width=48%}<br/>
left image shows the led_update() occuring -- right image shows a single led_update()<br/><br/></div>

</details>

_Code size: 651 bytes_

### Bitmap Fonts

A bitmap font is very much the same as a bitmap graphic.
This means, if we have a properly formatted font
small enough to fit the 6x7 charlieplexed LEDs of the **ACK1**
then we can display characters in the same way we display icons and other simple graphics.

The `includes/font.h` file contains the bitmaps for a 5x7 pixel font.
To save space, it only includes the ASCII characters starting with SPACE (0x20) up through TILDA (0xFF).
The data is stored in two formats.

- `font7x5` is as sequential 7 bytes of 5 bits representing the 7 rows of 5 columns of a character
- `font5x7` is as sequential 5 bytes of 7 bits representing the 5 columns of 7 rows of a character

The first representation is easier for processing the LEDs but uses more data storage.
For example, the letter **C** has 7 bytes represented by the image.

<div align="center">![Bitmap Font](files/bitmap_letter_c.jpg)<br/></div>

To copy a character into the bitmap buffer used by the `leds_update()` code, we just need to copy the bytes from the font data into the bitmap - much like we do for a custom graphic or icon.

```C
void leds_bitmap_letter(char c) {
    if ((c < ' ') || (c > 127))
        c = ' ';
    c = c - 32;    // font does not contain first 32 glyphs

    uint8_t *bitmap;
    bitmap = &(font7x5[c * 7]);    // each glyph is 7 bytes
    memcpy(_led_bitmap, bitmap, ROW_COUNT);
}
```

The 6x7 charlieplexed LEDs can only display one 5x7 character at a time. To display a text string, we want to scroll the characters.

There are two ways to easily create the appearance of scrolling text. Each method requires updating the bitmap uses by the charlieplexed LEDs the `leds_update()` code at some interval. The best range is between 50-75 milliseconds between shifting the columns. A single character scrolls into view in 6 intervals and scrolls out of view in 6 intervals.

**Method 1:** Direct manipulation of the LEDs bitmap. In this method, the application code must lock out `led_update()` and then shift all of the bytes of the bitmap left one position. Then copy one bit from each of the font character’s bytes into the newly vacated bits. This uses very little data but requires a lot of code shifting and masking bits.

**Method 2:** maintain a local copy of a bitmap the same size as the LEDs bitmap and perform all operations on the local copy. Then, simply copy the result the local copy to the LEDs bitmap. This is still nearly as complicated as the first method.

**Method 2 Optimized:** By using a local bitmap twice as wide as the LEDs bitmap, we simplify a lot of the masking. We treat each pair of 8-bits bytes as a 16 bit value. We shift it one position at easy interval. We copy out the left bytes to the LEDs buffer. On every 6th interval, the right bytes are all shifted out and we copy in a whole character.

The result is the following example code.

```C
void scroll_message(void) {
    if (_scroll_timer < milliseconds()) {
        _scroll_timer = milliseconds() + _scroll_speed;

        // if the counter reaches zero, load next character
        if (_scroll_counter == 0) {
            char *glyph;

            if (_scroll_message[_scroll_index] == 0) {
                if (_scroll_repeat)
                    _scroll_index = 0;    // start message over again
            } else
                _scroll_index++;          // next character in the message

            if (_scroll_message[_scroll_index] == 0) {
                glyph = get_letter_bitmap(' ');
                _scroll_counter = 12;     // double space for extended gap
            } else {
                glyph = get_letter_bitmap(_scroll_message[_scroll_index]);
                _scroll_counter = 6;
            }

            for (uint8_t i = 0; i < ROW_COUNT; i++) {
                _scroll_buffer[i].blocks[1] = glyph[i]; // set second block to character
            }
        }
        uint8_t buffer[ROW_COUNT];
        for (uint8_t i = 0; i < ROW_COUNT; i++) {
            _scroll_buffer[i].value = _scroll_buffer[i].value >> 1;    // scroll both blocks
            buffer[i] = _scroll_buffer[i].blocks[0];                   // copy first block to buffer
        }

        leds_bitmap_load(buffer);
        _scroll_counter--;    // decrement character column counter
    }
}
```

Look at `includes/scroll.h` and `includes/demo_scroll.h` for the complete example using the optimized method of scrolling a text string.

## Optional Exercises

Here are some optional exercises to try:

- Create a bitmap which is a hollow box of the outer most LEDs
  - Use the timer to switch between all the LEds on and just the hollow box on
- Create three bitmaps where each is a smaller box than the previous
  - Use the timer to switch between the boxes -large, medium, small, medium repeat

## Next ...

Once you have completed lesson 3 and are able to turn all the LEDs on and off,
continue on to [Lesson 4](lesson4.md) or return to the [syllabus](syllabus.md).
