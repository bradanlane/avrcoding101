# AVR Coding 101 - Lesson 5

<table border="0"><tr><td width="48%">
The goal of lesson 5 is to become familiar with the AVR microcontroller USART for serial data I/O.
By the end of this lesson, you should have accomplished the following:

- Understand how to setup simple USART for TX only
- Send status messages
- Setup _Interrupt Service Routines_ for both RX and TX
- Process input from RX and transmit output to TX
</td><td width="48%" align="center">
<a href="https://youtu.be/6-dJRLVPkz8" target="_blank"><img src="files/avrcoding101_lesson5.jpg" alt="AVR Coding 101 - Lesson 5"></a>
<br/>
<i>Companion Video of Lesson 5<br> (click to watch)</i>
<br/><br/>
</td></tr></table>

**IMPORTANT:** The use of interrupts services routines for USART require buffers and buffer management.
While the requisite code is _"just C programming"_, the implementation is somewhat detailed
and not educational for the purposes of this lesson.
All of the necessary circular buffer code is provided in `circular_buffers.h`.
This code will be used for the interrupt service routines.

[[_TOC_]]

## Understanding USART

USART = universal synchronous asynchronous receive transmit. Don't worry, it's the acronym from here on out.

While the AVR microcontroller support both synchronous and asynchronous, most applications use asynchronous. Synchronous mode requires the additional pins of `XCK` for clock and `XDIR` for direction.

For most conversations, USART is also referred to as _serial I/O_.

A common - _some would say lazy_ - use of USART is to output messages. This is a common debugging technique when a hardware debugger is not available. It's also the core of one of the most basic programs - "Hello World".

The "Hello World" program actually requires quite a bit of low level code. But it can be drastically simplified!

Rather than pontificate and regurgitate a lot of background, let's just jump to coding "Hello World" and learn the basics of USART on the AVR microcontroller.

## Programming Simple USART TX

For the "Hello World" example, we only need to transmit a string.
For the duration of sending the data, we don't need to be doing much else.
We don't even need the complexity of buffers or interrupt service routines.

### RX/TX Pins

The **ACK1** uses the default I/O pins for USART.
They are `PB2` for TX and `PB3` for RX and found in
Table 5.1 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf),

These are no different from any of our other I/O pins and are setup the same:

```C
void usart_setup(uint32_t baudrate) {
    // the default USART pins are 2 and 3 on PORTB
    PORTB.DIRSET = PIN2_bm; // TX pin is output
    PORTB.DIRCLR = PIN3_bm; // RX pin is input
}
```

### Calculating BAUD Rate

The microcontroller USART is a bit like a timer in so much as it need to switch an I/O pin as a specific speed in order to convert data to HIGH and LOW which are recognized by whatever is connected to receive the data.

Just like lesson 4 had a formula to convert a frequency to an interval,
we need a formula to convert a BAUD rate to a value for the USART.

```C
#define BAUD_RATE_FORMULA(b) ((((8 * F_CPU) / b) + 1) / 2)
```

If you look up the BAUD formula from Microchip &reg;, it will look different.
The results are identical _(mine is just more efficient)_.

```C
#define USART0_BAUD_RATE(BAUD_RATE) ((float)((F_CPU) * 64 / (16 * (float) BAUD_RATE)) + 0.5)
```

Some microcontrollers have multiple USARTs.
The ATtiny1616 has only one but to keep code consistent,
it uses the same naming conventions, thus it uses `USART0`.

The AVR microcontroller USART has its own registers and memory locations for its settings.
In lesson 1, we covered how the microcontroller memory locations are similar to C programming structures.

This `USART0` refers to a block of memory.
Within this block are data locations for the settings we need to affect.
There is also a memory location within this block for a byte of data to be sent.
The AVR header file define labels for all of the locations which makes the code much more readable. For example, the calculated value for BAUD is stored in `USART0.BAUD`.

```C
void usart_setup(uint32_t baudrate) {
    // the default USART pins are 2 and 3 on PORTB
    PORTB.DIRSET = PIN2_bm; // TX pin is output
    PORTB.DIRCLR = PIN3_bm; // RX pin is input

    // set  baud rate
    USART0.BAUD = (uint16_t)BAUD_RATE_FORMULA(baudrate);
}
```

### Ready to Transmit

The last step is to enable TX.

```C
void usart_setup(uint32_t baudrate) {
    // the default USART pins are 2 and 3 on PORTB
    PORTB.DIRSET = PIN2_bm; // TX pin is output
    PORTB.DIRCLR = PIN3_bm; // RX pin is input

    // set  baud rate
    USART0.BAUD = (uint16_t)BAUD_RATE_FORMULA(baudrate);

    USART0.CTRLB |= USART_TXEN_bm; // enable TX
}
```

That's it. The USART is ready to transmit data!

The `USART0` has a memory location for data to send - `TXDATAL`.
Each time we want to transmit data, we need to be sure the data register is empty before we put a byte of data into it.

The USART has a `STATUS` register which has the _(Data Register Empty Interrupt Flag)_ - `USART_SPI_DREIF_bm`.

To transmit a byte we wait for the status flag to be set and then we load 1 byte of data into the TX register.

```C
void usart_put_byte(uint8_t data) {
    // wait for any previous TX to be finished
    while (!(USART0.STATUS & USART_DREIF_bm)) {
        ;
    }
    // put new byte into the TX data register
    USART0.TXDATAL = data;
}
```

That's it!

It might also be handy if we have a helper function to transmit an entire string of data.

```C
int usart_put_string(const char *text) {
    // no error handling
    uint16_t len = strlen(text);

    for (int i = 0; i < len; i++)
        usart_put_byte((uint8_t)text[i]);

    return len;
}
```

**Exercise:** Edit `main.c` to send "Hello World" when a button is pressed.

To see the output in VS Code, click on the _electrical plug icon_ in the lower left tool bar.

If you don't see text when you press the button, check if you have changed the <kbd>UART UPDI</kbd> switch to UART.

Also, don't forget to switch it back before you try to flash your program code again.

_Code size: 269 bytes_

## Programming USART TX as an ISR

There may be no reason to convert the simple TX code to an interrupt unless you
really need it to run in the background. This is less likely at higher BAUD rates.
Regardless, let's step through how to do it ... _for completeness_.

### Buffers

The USART interrupt services routines need to read/write to buffers since our program code will not be constantly polling.

Rather than spend a lot of time writing buffer functions, this lesson provides `usart_buffers.h` with full implementations of circular buffers.

```C
#define UART_TBUFFER_SIZE 64    // set to 0 to disable TX buffers
#define UART_RBUFFER_SIZE 128   // set to 0 to disable RX buffers
#include "usart_buffers.h"

/*
    The important functions are:

        void buffers_init(void);
        int buffers_tx_get_byte(void);
        int buffers_tx_put_byte(uint8_t b);
        uint16_t buffers_tx_get_length(void);
        int buffers_rx_get_byte(void);
        int buffers_rx_put_byte(uint8_t b);
        uint16_t buffers_rx_get_length(void);
*/
```

Whe we want to transmit data, we just add it to the buffer.
The interrupt service routine will take data out of the buffer.

Our `put` function now use the buffers.

```C
int usart_put_byte(uint8_t data) {
    // put a byte onto the circular buffer for TX data
    int rtn = buffers_tx_put_byte(data);

    // magic needs to happen here

    return rtn;
}
```

_We will get to that magic in a moment._

### TX Interrupt Service Routine

All of the setup is unchanged from the simple example.

Just like the interrupt service routine in lesson 2 had a specific name,
the same is true for our USART ISRs.

The simple TX example polled the _Data Register Empty_ flag.
With an Interrupt service routine, it handles that for us.
Here is our empty routine:

```C
// USART Transmit 'Data Register Empty' Interrupt Handler
ISR(USART0_DRE_vect) {
    // ...
}
```

The ISR will be called as soon as `DRE` is clear.
But that exposes a problem. What does the interrupt service routine do if there is no data to transmit.
The proper action is to disable the ISR until there is data to transmit.
Enable it when there is data to transmit.
Then disable it again when there is no more data.

```C
// convenient macros which improve code readability
// since there are no convenient SET and CLR registers, we must use bitwise operations
#define USART_ENABLE_TX     { USART0.CTRLA |=  (USART_DREIE_bm); }
#define USART_DISABLE_TX    { USART0.CTRLA &= ~(USART_DREIE_bm); }
```

Let's combine the buffer functions with these macros and finish the ISR.

```C
uint8_t _usart_tx_idle; // we need to know if the ISR is active or not

// USART Transmit 'Data Register Empty' Interrupt Handler
ISR(USART0_DRE_vect) {
    // "data register empty", send the next byte
    if (buffers_tx_get_length()) {
        // send byte from top of buffer
        USART0.TXDATAL = (unsigned char)buffers_tx_get_byte();
    } else {
        // buffer is empty, turn off interrupt
        _usart_tx_idle = true;
        USART_DISABLE_TX;
    }
}
```

The ISR should only be enabled where there is data to transmit.
We need a global flag to know if the ISR is enabled or disabled.

Now for that _magic_ back in our `usart_put_byte()` function.
The complete function needs to know if the ISR is disabled.
If it is, then it needs to enable it.

```C
int usart_put_byte(uint8_t data) {
    // put a byte onto the circular buffer for TX data
    int rtn = buffers_tx_put_byte(data);

    if (rtn >= 0) {
        // if we have been idle, re-enable the ISR to start transmitting
        if (_usart_tx_idle) {
            _usart_tx_idle = false;
            USART_ENABLE_TX;
        }
    } else {
        // there was a problem
    }

    return rtn;
}
```

It should now be a bit clearing. When we put data into the buffer for transmitting,
we check if the interrupt service routine is idle. If it is, then we enable it.

In the ISR, if we run out of data to transmit, we set the idle flag and disable the ISR.

## Programming USART RX as an ISR

USART TX could easily be handling with the simple polling code.
However, RX is a different matter.
There is no way to know when (and how much) data might be received.
The USART RX needs an interrupt service routine and a buffer.

### Buffers (redux)

We covered the circular buffers.
The only thing to consider is the size of the buffer for receiving data.
It should be as small as possible but needs to be big enough for all the data which
could be received before the program has a chance to remove it from the buffer.

```C
int usart_get_byte() {
    // get a byte from the circular buffer of RX data
    return buffers_rx_get_byte();
}
```

### RX Interrupt Service Routine

The USART setup needs to be updated to handle the RX.
We have two new helper macros to enable/disable the RX ISR.
We also add a small global counter to let us know if we received
data but there was no room in the buffer to store it.
This isn't absolutely necessary but its helpful for development and debugging.

```C
#define USART_ENABLE_RX     { USART0.CTRLA |= (USART_RXCIE_bm); }
#define USART_DISABLE_RX    { USART0.CTRLA &= ~(USART_RXCIE_bm); }

uint8_t _usart_rx_overflow; // 'RX buffer is full' counter

void usart_setup(uint32_t baudrate) {
    buffers_init();

    // the default USART pins are 2 and 3 on PORTB
    PORTB.DIRSET = PIN2_bm; // TX pin is output
    PORTB.DIRCLR = PIN3_bm; // RX pin is input

    // set  baud rate
    USART0.BAUD = (uint16_t)BAUD_RATE_FORMULA(baudrate);

    USART0.CTRLB |= (USART_TXEN_bm) | (USART_RXEN_bm); // enable TX and RX

    USART_ENABLE_RX; // enable RX interrupt

    _usart_tx_idle = true;  // the TX ISR is disabled to start
    _usart_rx_overflow = 0; // the RX buffer is a fixed size

    sei(); // enable all interrupt handlers
}
```

When a byte of data is received by the microcontroller, it is stored in `RXDATAL`.
Our interrupt service routine for RX data is called when the hardware knows _receive complete_ `RXC`.

We need to get the data and add it to the buffer.
For good measure, we update the `overflow` counter if the buffer is already full.

```C
// USART Receive Complete Interrupt Handler
ISR(USART0_RXC_vect) {
    unsigned char c;

    // get received char
    c = USART0.RXDATAL; // this also clears the interrupt flag for us

    // put received char in circular buffer
    // check if there's space
    if (buffers_rx_put_byte(c) < 0) // -1 means the 'put' failed
        _usart_rx_overflow++;       // no space in buffer
    else
        _usart_rx_overflow = 0;
}
```

**Exercise:** Update `main.c` to check for data received.
To demonstrate your RX and TX are working, uppercase a lowercase letter
and lowercase an uppercase letter. The transmit it back.

Don't forget, you can include the code from the previous lessons:

```C
#include "cpu.h"
#include "milliseconds.h"
#include "buttons.h"

// ... rest of your code ...

```

To see the output in VS Code, click on the _electrical plug icon_ in the lower left tool bar.

If you don't see text when you press the button, check if you have changed the <kbd>UART UPDI</kbd> switch to UART. (Also, don't forget to switch it back before you try to flash your program code again.)

**Disclaimer:** This is a pretty detailed topic with a lot of moving parts.
If you learn more quickly from examining working code, then take a look at `lesson5.h` which contains the final code for this exercise.

_Code size: 959 bytes (214 bytes of data)_

## Optional Exercises

Here are some optional exercises to try:

- Use the Serial output to display a text question which has a yes/no answer
  - Wait for input and test if the answer is 'yes' or 'now'
  - Output when the user has entered the correct answer
- Use the Serial output to display ascii art

## Next ...

Once you have completed lesson 5 and are able to
transmit and receive data over a serial connection,
continue on to [Lesson 6](lesson6.md) or return to the [syllabus](syllabus.md).
