# AVR Coding 101: Hardware

[[_TOC_]]

## Introduction

The **AVR Coding 101** material uses the **ACK1** (AVR Coding Kit). This kit consists of the ACK1 hardware board and the CH340E USB-UART adapter. The **ACK1** is [available from Tindie](https://www.tindie.com/products/bradanlane/ack1-avr-coding-kit/).

Anywhere in the documentation making reference to the **ACK1** is referring to the combination of the CH340E connected to the ACK1 hardware board.

You will need a USB data cable compatible with your computer and with a USB-C on one end to connect to the CH340E USB-UART adapter.

The 6-pin pre-solder pin connector of the CH340E will plug into the staggered header connector of the ACK1 hardware board. The staggered holes means the CH340E can be connected and removed from the ACK1 hardware board without the need for soldering.

## Features

<div align="center">![AVR Coding Kit ACK1](files/ACK1_diagram.png)</div>

The ACK1 hardware board has a staggered 6-pin header located at the lower left of the board.
This is the location for connecting the CH340E USB-UART adapter.
_(Please note, the RX/TX labels refer to the RX and TX of the CH340E. If you wish to use a 3rd party UPDI programming adapter, use the RX pin as UPDI.)_

Directly above the CH340E connection is a switch labeled `UART-UPDI`. This will switch the connection of the CH340E between connecting to the microcontroller USART pins and its UPDI programming pin. This switch must be in the UPDI position to program the microcontroller.

At the top left of the **ACK1** is a header which breaks out several general purpose I/O pins of the microcontroller. The first and last pins (PB5 and PC3) also connect to the push buttons. The breakout pins have been specifically chosen as they provide access to I2C and SPI for user projects and future lessons.

To the immediate right fo the breakout pins is a 12mm square piezo buzzer. This buzzer has been chosen because it produces reasonable volume, even when the **ACK1** is powered by the CR2032 battery.

The upper right portion of the **ACK1** contains 42 red LEDs in a 6x7 matrix. The LEDs are charlieplexed and collectively share 7 general purpose I/O pins of the microcontroller.

The lower right portion of the **ACK1** has a battery holder for a CR2032 coin cell.

Along the lower edge of the **ACK1** is an ON/OFF switch for the battery.

Immediately above the ON/OFF switch are two push buttons which are connected to general purpose I/O pins of the microcontroller.

The microcontroller used on the **ACK1** is the Microchip ATtiny1616. This chip has 17 available general purpose I/O pins, 16KB of flash storage, 2KB of RAM, and 256 bytes of EEPROM. The flash is rated at 10,000 erase/write cycles. The EEPROM is rated for 100,000 erase/write cycles.


_For extended details of the microcontroller, please refer to the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)._

## Usage

The lesson materials include the files for programming the **ACK1** using VS Code and PlatformIO. The lessons do not use the Arduino framework _(although there is an example of lesson-1 provided for comparison)_.

Programming the ACK1 hardware board requires a UPDI programmer. The simplest form of UPDI programmer is a serial (UART) adapter in combination with the `pymcuprog` software. The [prerequisites](prereq.md) instructions cover installing VS Code, PlatformIO, and pymcyprog.

To flash code to the ACK1 hardware board, using the provided CH34E UART seral adapter, the **UART UPDI** switch on the **ACK1** must be in the **UPDI** position. _(Attempting to flash the **ACK1** when the switch is set for **UART** won't work. Trust me, I have made this mistake many times.)_

## Technical Details

The **ACK1** design is open source and the [schematic](files/ACK1_Schematic.pdf) and [bill of materials (BOM)](files/ACK1_BOM.pdf) are included as PDFs.

<div align="center">![AVR Coding Kit ACK1 Schematic](files/ACK1_Schematic.jpg)</div>

## Next ...

Once you are familiar with the **ACK1** hardware, continue on to the [prerequisites](prereq.md) or
return to the [Syllabus](syllabus.md).
