# AVR Coding 101: Prerequisites

The course materials assume the use of the **ACK1** hardware kit, the VS Code editor and development environment, PlatformIO, and pymcuprog.

The **ACK1** board is [available from Tindie](https://www.tindie.com/products/bradanlane/ack1-avr-coding-kit/).

[[_TOC_]]

## Software Installation

_(Users may want to watch [a video](https://youtu.be/BOwe9JwnJZw) of the steps described here.)_

The materials use VS Code, PlatformIO, and pymcuprog. The order of installation is important as each one builds on its predecessor. The software may be installed from the following links or commands:

[VSCode](https://code.visualstudio.com/)
_(on Linux, these [instructions](https://code.visualstudio.com/docs/setup/linux)
will install and update VScode using the package manager)_

[PlatformIo](https://platformio.org/install/ide?install=vscode) is installed within VSCode
as extension _(the toolbar on on the left; the icon with 3+1 squares)_
Search for PlatformIO. Click _Install_. It may request to restart VS Code.

[pymcuprog](https://github.com/microchip-pic-avr-tools/pymcuprog) must be installed **within** VSCode.
Use the _square and angle bracket_ Terminal icon in the toolbar at the bottom. This opens a terminal within the VSCode environment.
Use the command `pip install pymcuprog`.

[AVR Coding 101](https://gitlab.com/bradanlane/avrcoding101) is the actual course material _(and this repository)_.
Please download a ZIP of the repository and expand it to a folder on your computer.

## Software Verification

Please verify all the required software has been installed.
There are two steps which should verify everything.

To verify the development environment:

1. Connect the CH340E to the ACK1 hardware board and connect the CH340E to your computer with a USB cable.
  -_For the remainder of the documentation, the combination of the CH340E and the ACK1 hardware board will be referred to simply as the **ACK1**._
  - If you have not received your **ACK1**, you may still verify your environment by using any Serial device connected to your computer in place of the references to the **ACK1** in the remainder of the instructions. The actual upload step will fail but that is not critical at this time._
1. Locate the Serial device assigned to the **ACK1** on your computer _(eg: COM3 on Windows, /dev/ttyUSB0 on Linux, and I have no clue on Mac)_
1. Launch VS Code
  1. Open the folder where you expanded the ZIP of these course material
    - Use the command _File -> Open Folder ..._
    - You should see all of the files and folders in the explorer panel at the left side of VS Code
    - **IMPORTANT:** The first time you open the **AVR Coding 101** folder in VS Code, PlatformIO will detect the project and install the necessary tools. This may take several minutes. Wait until it has completed. _(see photo below)_
  1. Connections
    - Connect a USB-C cable to the little purple CH340 UART adapter.
    - Connect the other end of the USB cable to your computer. _(a red LED should light on the CH340 board)_
    - Plug the downward facing pins from the CH340 adapter into the staggered pins on the **ACK1**. _(the CH340 should be extending off the left side of the **ACK1**)_
  1. Check your computer devices to identify the name given to your new Serial/COM port.
    - Windows will uses a name like `COM3`, etc
    - Linux will use a name like `/dev/ttyUSB0` or `/dev/ttyASM0`
    - MacOS will use a name like `/dev/tty.usbserial-1440`
  1. Edit the `platformio.ini` file to ensure the following
      - Change the value assigned to `upload_port` to match your system's Serial device assigned to the **ACK1**.
      - Ensure `upload_protocol = custom` is present.
      - Ensure `upload_command = pymcuprog write -t uart -d attiny1616 -u $UPLOAD_PORT -c $UPLOAD_SPEED -f $SOURCE --erase --verify` is present.
      - More information about `platformio.ini` configuration can be found [here](./platformio.ini).
  1. Build the project `main.c`
    - Use the _checkmark_ icon in the bottom left toolbar (or use `CTRL+ALT+B`)
  1. Locate the <kbd>UART-UPDI</kbd> switch on the ACK1 and make sure it is set to `UPDI`
  1. Upload the firmware to the **ACK1**
    - Use the _right arrow_ icon in the bottom left toolbar (or use `CTRL+ALT+U`)
    - The blue LED on the CH340 UART adapter will flash briefly
    - the code will begin executing on the **ACK1** immediately _(the test program does nothing so there will be no obvious indication it is executing)_

Once PlatformIO is installed and the project folder has been opened, there will be a delay as PlatformIO installs the required tools. When It has completed, you should see the following  toolbar at the bottom left of VS Code.

<div align="center">![PlatformIO Toolbar](files/toolbar_diagram.jpg)<br/></div>

In addition to this list of software, the lessons will make extensive reference to the the Microchip &reg; ATtiny1616  [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf).
You should download the PDF to a location on your computer that will be convenient for quick reference.

## The ACK1 Hardware

The lessons have been designed and written based on the AVR Coding Kit (**ACK**).
While other hardware may be used to complete the programming exercises, the **ACK1** has all of the required features on a single board.

## Next ...

Once you have completed the prerequisites, you may return to [syllabus](syllabus.md) to begin the lessons.
