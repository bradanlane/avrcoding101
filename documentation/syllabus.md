# AVR Coding 101: Syllabus

Welcome to the class material for **AVR Coding 101**
and the companion hardware, the **ACK1** _(AVR Coding Kit)_, [available from Tindie](https://www.tindie.com/products/bradanlane/ack1-avr-coding-kit/).

<div align="center">![AVR Coding Kit ACK1](files/ACK1_diagram.png)</div>

The **AVR Coding 101** lessons provide lessons, exercises, and sample code for programming the Microchip&reg; AVR microcontroller hardware without 3rd party frameworks or vendor libraries.

## Introduction

Coding for a microcontroller falls into one of three categories:

- Coding to a framework
- Coding to a vendor library
- Coding to the hardware

Arduino is a common framework for programming microcontrollers. With Arduino, the code a developer writes is similar, regardless if the target is a Microchip&reg; ATmega328, an Espressif ESP32, or a Raspberry Pi RP2040. The framework provides an abstraction layer to the hardware. However, that abstraction layer can add a lot of extra code. It can also impose limitations of the developer since the framework uses features of the microcontroller and may not expose them.

Vendor libraries avoid much of the overhead of frameworks because they are purpose built for the microcontroller. More and more vendors are offering libraries for their own hardware. A growing number of vendors are also providing the tools for building software for their microcontrollers and even providing "code generation". These tools may lock the developer into a specific computer platform. The code generation adds abstractions and hides how the microcontroller works. It can be difficult for the developer to understand what the vendor code is doing vs their own code.

Coding to the hardware requires intimate knowledge of the microcontroller architecture. There is often a steep learning curve. The developer needs to read through and understand the microcontroller datasheet. These datasheets can be hundreds of pages. Even the datasheet will not have all details the developer needs. The microcontroller manufacturer may publish "Getting Started" guides but these may follow the availability of a microcontroller by years.

The advantage to coding to the hardware is performance, code size, and customizability. The developer has total control over the hardware and chooses what is needed and what is not.

The **AVR Coding 101** lessons cover the basics of coding the the AVR microcontroller hardware.

## Lesson Plan

The **AVR Coding 101** lessons cover:

- The **ACK1** hardware kit
  - the ATtiny1616 features
  - the CH340E USB-UART adapter
- AVR coding syntax
- Turning on/off an LEDs attached to I/O pins
- Reading a button from an I/O pin
- Creating a timer (TCB0) and updating a variable in an interrupt service routine (ISR)
- Controlling charlieplexed LEDs
  - Using a timer (TCB0) to refresh a matrix of LEDs
  - Creating a simple display buffer
  - Calculating refresh rates
- Creating a timer (TBA0) to control an I/O pin to make a tone
- Implementing Serial I/O using the USART interrupt service routines
- Storing data in EEPROM
- Reading the microcontroller signature
- Reading and updating the microcontroller configuration FUSES

**Note:** All of the lessons will be written using the C programming language. The lessons will not attempt to teach C programming. For those new to C programming, there are many online resources including a **codecademy** [course](https://www.codecademy.com/learn/learn-c).

## Repository Structure

The structure of this repository is as follows:

- documentation:
  - [Syllabus](syllabus.md) Lesson plan
  - [ACK1](ack1.md) Overview of the hardware kit
  - [Prerequisites](prereq.md) Software Installation
  - [Lesson 1](lesson1.md) Blink
  - [Lesson 2](lesson2.md) Timer Interrupt for LEDs
  - [Lesson 3](lesson3.md) Charlieplexing LEDs
  - [Lesson 4](lesson4.md) Timer Controlled I/O Pin for Making Tones
  - [Lesson 5](lesson5.md) UART simple output and interrupts for I/O
  - [Lesson 6](lesson6.md) EEPROM, Signature Bytes, and FUSES

- files: _used for the documentation_

- include: _lesson material converted to reusable code_

- lib: _not used_

- src: the code for each lesson\*
  - main.c: user code goes here
  - lesson0.h: empty starter code for lessons
  - lesson*.h: example of code for each lesson

 \* The final code for each lesson is provided as a `.h` file.
 Subsequent lessons will include the reusable modules from previous lessons.

 **RTFM:** Write your code in the `main.c` file. The contents of the `lesson*.h` files are for reference so you may compare your results.
