# AVR Coding 101: Lesson 2

<table border="0"><tr><td width="48%">
The goal of lesson 2 is to become familiar with the AVR microcontroller timer.

By the end of this lesson, you should have accomplished the following:

- Understand how to setup TCB0 to repeat on an interval
- Setup an _Interrupt Service Routine_ to perform a task at each interval
- Blink an LED using the timer
</td><td width="48%" align="center">
<a href="https://youtu.be/WJqW8Xn-qXI" target="_blank"><img src="files/avrcoding101_lesson2.jpg" alt="AVR Coding 101 - Lesson 2"></a>
<br/>
<i>Companion Video of Lesson 2<br> (click to watch)</i>
<br/><br/>
</td></tr></table>

[[_TOC_]]

## Understanding Timers and Interrupts

Most microcontrollers have one or more timers.
The ATtiny1616 has five - three are general purpose and two have special applications.
The three general purpose timers are `TCA0`, `TCB0`, and `TCB1`. The special purpose are `TCD0` and the `RTC` counter.

### Timer Resolution aka Accuracy

On the Microchip &reg; AVR microcontrollers, these timers are most often 8-bits or 16-bits.
_(But wouldn't you know it, the ATtiny1616 also has a 12-bit timer)._

- 8-bit timers have a maximum value of 255 before starting back at 0
- 12-bit timers have a maximum value of 4,095 before starting back at 0
- 16-bit timers have a maximum value of 65,535 before starting back at 0

An 8-bit timer is sufficient for tasks with simple fixed periods or needing a simple ratio _(like varying the percentage of a pulse width signal)_.
The 16-bit timer provides more precision _(for tasks like generating an audible frequency in lesson 4)_.

The amount of time it takes for a timer to reach its interval is dependent on the speed of the CPU (`F_CPU`).
For example, if we are using a 16-bit timer which has a maximum value of 65,535
and our microcontroller is operating at 20Mhz,
then the maximum duration of the timer is 65535 / 20,000,000 or 0.0033 seconds, aka 3.3 milliseconds.

That's not very much time.

To solve timers having such a short duration, _some timers_ include a _prescaler_ - aka a divider.
Using a prescaler of 1024, the previous equation becomes 65,535 / (20,000,000 / 1024) or 3.38 seconds.

If a timer does not support a prescaler, then an alternative is execute a small piece of code at each interval and only execute the significant code every Nth time.

### Timer Behavior

A timer has two options for _doing something_.
A timer may be configured to execute code or it may change the state of an I/O pin.
_(It can also be configured to do both)._
There are variations of these two behaviors
_(such as monitoring the state of a pin, or even splitting a 16-bit timer into two 8-bit timers)_
but, for the most common usecases, we will stick to these two behaviors.

#### Timer Interrupt Service Routine

In lesson 1 we covered I/O registers. These are accessed through assigned locations.
To make coding easier, the manufacturer has provided a header file will labels, names of variables, and definitions of data structures for all of these fixed locations in the memory map.

The code executed on each interval of a timer is called its _interrupt service routine_ or ISR.

Each ISR has a specific pre-defined reference location.
Some timers have multiple possible actions available so there are multiple ISR references.

An ISR will execute on every interval.
The more frequent the interval the more important it is to keep this code small.
The amount of code which can be executed is very dependent on the interval the timer has been configured to use and the microcontroller CPU speed.

**Note:** When the `F_CPU` is reduced, the maximum duration of a timer increases but so does the time required to execute instructions.

### Timer Advanced Options

There is another option for creating a longer timer duration - use a different oscillator!
In the formula, we used the internal 20MHz oscillator.
The ATtiny1616, and other microcontrollers, have another internal oscillators and support an external oscillator.
The second internal oscillator is an ultra low power oscillator running at only 32KHz.
Using this oscillator, the maximum duration of the timer is 65,535 / 32,000 or 2.05 seconds.
_(We will not be availing ourselves of this option.)_

## Programming a Timer

A timer has several properties including its _mode_, its _interval_, and its _action(s)_.
The most basic combination of these three properties is "perform an action, after a specified interval, forever".

This is the usecase for lesson 2.

_(There is much more complexity for timers but let's not try to eat an elephant just yet.)_

### Creating a Simple Timer

For basic timers - which need to count to some value and repeat - the _some value_ needs to be between zero and the maximum value supported by the timer.

The Microchip &reg; ATtiny1616 has three timers: `TCA0`, `TCB0`, and `TCD0` plus the `RTC` counter.

Both `TCA0` and `TCB0` are 16-bit timers. `TCA0` supports a number of prescaler values
while `TCB0` has only a 2X prescaler option.

_(We need prescaler support for a future lesson so we will use `TCB0` for this lesson)._

We will configure `TCB0` to create a reoccurring event.
This is called _Periodic Interrupt Mode_ and is described in section 21.3.2 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf).

In this case, the event we will create is to count up from zero,
comparing the current value of the timer to our choses number.
When it matches, the interrupt code is executed and the timer is reset back to zero and repeats.

**NOTE:** Rather that to go into complete detail
about all the possible uses and configuration of each timer,
the following example will provide a description and code for the _Periodic Interrupt_ usecase.
_Those preferring self flagellation are encouraged to read pages 176 thru 315 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)._

### Configure Timer TCB0

In lesson 1, we used a button to turn an LED on.
Now let's create a timer so we can turn the LED on or off every second.

We know the ATtiny1616 has a default CPU frequency of 20,000,000 cycles each second.
We want an interval of 1 second.
We calculate 20,000,000 / 1 or 20,000,000 for the period of our timer.
However, 20,000,000 is much larger than the maximum value of a 16 bit timer
which is only 65,535.

Let's config the timer for 1 millisecond.
We can toggle the LED every 1000 times the timer occurs.

This approach has a convenient side effect.
If we update a global variable with the timer,
then we have a convenient 1 millisecond timer we can use for lots of purposes.

We calculate 20,000,000 / 1,000 or 20,000 for the period of our timer.
The good news is that 20,000 is less than the maximum 16-bit value of 65,535.

The timer mode we want to use is called _Capture/Compare_ or `CCMP`.

AVR timers have fixed locations within the microcontroller memory map.
The `TCB0` reference is the start of a block memory for the data associated with the timer.
This block contains several elements.
We access individual elements using the notation described in lesson 1.

For example, the `TCB0` has an element for holding the maximum value we want the counter to reach before resetting.
This element is `CCMP`. We set the element by referencing it relative to the timer, as `TCB0.CCMP`.

<div align="center">![TCB0 Memory Map](files/tcb0_structure.png)<br/>
<i>The TCB0 timer has several elements to control its behavior</i>
<br/><br/></div>

We need to set the compare/capture value of `TCB0` to the value we calculated of 20,000.

```C
#define INTERVAL (F_CPU / 1000) # 20,000,000 / 1,000 = 20,000

void milliseconds_setup(void) {
    // the timer starts counting at zero so we use our calculated value -1
    TCB0.CCMP = (INTERVAL - 1);         // set the compare/capture register to our desired value
}
```

We want to be able to execute some code each time `TCB0` reaches our target value.

```C
void milliseconds_setup(void) {
    // the timer starts counting at zero so we use our calculated value -1
    TCB0.CCMP = (INTERVAL - 1);         // set the compare/capture register to our desired value

    TCB0.CTRLB = TCB_CNTMODE_INT_gc;    // timer mode: periodic interrupt; this is actually the default
}
```
_(more details of `CTRLA` and `CTRLB` may be found in section 21.5 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)._

We now enable our interrupt service routine and start the timer

```C
void milliseconds_setup(void) {
    // the timer starts counting at zero so we use our calculated value -1
    TCB0.CCMP = (INTERVAL - 1);         // set the compare/capture register to our desired value

    TCB0.CTRLB = TCB_CNTMODE_INT_gc;    // timer mode: periodic interrupt; this is actually the default

    TCB0.INTCTRL = TCB_CAPT_bm;         // enable the capture interrupt service routine
    TCB0.CTRLA = TCB_ENABLE_bm;         // enable the timer

    // [re]enable system wide interrupts; without this, the timer will run but never execute the ISR
    sei();
}
```

There is still something missing - the actual code that is executed when the timer interval is reached.

#### Writing a Interrupt Service Routine

An _interrupt service routine_ (ISR) is a piece of code which executes on an event.

**Note:** The block of memory for ISR addresses is called the _vector table_.

That may sound a bit confusing.

What is important, is each interrupt service routine has a specific name and is coded in a specific way to be used by the timer.
The microcontroller header file provides names and macros to hide some of the gorpy details.

The microcontroller needs to know which interrupt service routine we have written and it needs to update its table with the location of our code.

The periodic mode of `TCB0` uses the name `TCB0_INT_vect` for its interrupt service routine.
The `ISR()` directive is used to instruct the compiler to connect our code to the memory location where the microcontroller will look to find the code to execute.

The interrupt service routine for the _periodic interrupt mode_ event of the TCB0 timer looks like this:

```C
ISR(TCB0_INT_vect) {
    // do something
}
```

This represents the code which will be executed each time the timer reaches the compare/capture value we have set.

Regardless of what code we write in our interrupt service routine, we need to end by telling the microcontroller we are done.

```C
ISR(TCB0_INT_vect) {
    // do something

    TCB0.INTFLAGS = TCB_CAPT_bm;        // clear the interrupt flag
}
```

We are creating a 1 millisecond timer.
We want to be able to use the timer value elsewhere in our program.
We create a global variable and increment it within the interrupt service routine.

```C
volatile uint32_t _timer_counter;

ISR(TCB0_INT_vect) {
    _timer_counter++;

    TCB0.INTFLAGS = TCB_CAPT_bm;        // clear the interrupt flag
}
```

The `_timer_counter` uses the largest simple integer value available which is a 32-bit value.
We don't need negative numbers so it is declared as `unsigned`.
The result is a counter with a maximum value of 4,294,967,295.
This is a little less than 50 days.

**NOTE:** Two notes actually ... First, we use the prefix of `volatile` on variables when they are changed within an interrupt service routine to prevent the compiler from getting overly aggressive with optimization.
Our global variable could be named anything.
It is often not necessary but if the code in an ISR is misbehaving, adding `volatile` is a good step to debug if the problem in related to compiler optimization.
Second, adding an underscore at the beginning of a global variable name is a common practice to remind us it should only be referenced locally.
There is nothing stopping it from being accessed everywhere so this is really just a reminder to ourselves and other developers.

#### Adding some Access Functions

The `_timer_counter` global variable could be access directly. However, it may be better to only use it directly within our timer code. We can create a few helper functions to be used for the rest of our program.

```C
// a convenient reference for measuring the passage of time
uint32_t milliseconds(void) {
    return _timer_counter;
}

// a simple function to delay steps in a program
void milliseconds_delay(uint16_t ms) {
    uint32_t timer = milliseconds() + ms;
    while (timer > milliseconds()) {
        // spin wheels
    }
}
```

**Quiz:** What is wrong with our `milliseconds()` function?

<details>

<summary><b>Answer ...</b></summary>

The `milliseconds()` function is reading our `_timer_counter` variable.
It is possible for the interrupt service routine to be writing to the variable while we are trying to read it.
This would result in the value being unpredictable.

The solution is to prevent the interrupt service routine from changing the value at the same time the function
is reading the value. The method it to mark the code as `ATOMIC` meaning it must all be performed without interruption.

Here is the improved code:

```C
// a convenient reference for measuring the passage of time
uint32_t milliseconds(void) {
    uint32_t t;

    // an 'atomic block' will execute in its entirety without interruption
    // the `ATOMIC_RESTORESTATE` insures the system is as it was before the block

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        t = _timer_counter;
    }

    return t;
}
```

</details>

## Control an LED with a Timer

Now that we have a timer and an interrupt service routine to execute code on a periodic interval,
let's use it to control the LED.

Most of the code is already done.
We just need to change our LED on/off button code from lesson 1 to use the milliseconds value.

### Toggle the LED Every Second

The timer is configured for 1,000 times each second.
That is too fast to see if we are turning the LED on or off each time the service routine executes.

If we want to blink the LED once every second, then we can check to see if the counter has incremented 1000 times.
A trick is to use the _Modulo_ operation as in `A Modulo B` which is written as `A % B`.
_(Modulo represents the remainder of the division operation. It's a convenient way to test if `A` is evenly divisible by `B`)_

>When using Modulo to test for a value being perfectly divisible by another value - e.g. `((A % B) == 0)`,
>it is possible to skip over the condition when running a lot of code and not testing often enough.
>A better solution is to maintain a _counter_ variable and compare it against the `milliseconds()` value.

We could test the global variable inside the interrupt service routine and update the LED.
However, now that we have a `milliseconds()` function, we can test for our condition within our `main()` code.

```C
int main(void) {
    // setup goes here ..

    while (1) {
        if ((milliseconds() % 1000) == 0) {
            // every 1000 milliseconds, change the LED
            led_toggle();
        }
    }
    return 0;
}
```

**Exercise:** Edit `main.c` from Lesson 1 to configure TCB0 as a timer and add the interrupt service routine. Then use the `milliseconds()` value to toggle the first LED (upper left) on/off once each second.

### Timers and the Main Clock

Does the LED change every second? Yeah, probably not.

Our default for the _Main Clock_, used by the timer, is the 20MHz internal reference.
We calculated the interval for the interrupt using 20MHz. So what is the problem?

>Diagnosing timers can be tricky because they often occur very quickly.
>Print statements would take too long. Even a real code debugger is too slow.
>We want something that can show us changes _as they happen_ and thousands of times in a second.
>An oscilloscope is a handy tool for diagnosing odd behavior in timers.
>Toggling an I/O pin is easy, is very very fast, and adds very little code.

If we setup one of the I/O pins as output and toggle it within the interrupt service routine, we can attach
an oscilloscope to that pin (and to GND).

```C
    // setup a pin for use in our ISR
   PORTC.DIRSET = PIN2_bm;  // for debugging we set a spare pin as output
```

```C
    // inside the ISR, toggle the pin every time the ISR occurs
    PORTC.OUTTGL = PIN2_bm; // for debugging we toggle a spare pin every interrupt
```

Compare the following two images:
<div align="center">![Oscilloscope 6ms](files/oscilloscope_6ms.png){width=48%} ![Oscilloscope 1ms](files/oscilloscope_1ms.png){width=48%}<br/>
left image is what have -- right image is what we want<br/><br/></div>

We see our interrupt service routine is every 6 milliseconds rather than every 1 millisecond.
This is a useful clue.

Let's look at the specifics of the main clock (section 10.2) in the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf).

The main clock may be configured to use one of several sources.
The main clock also supports prescalers.
We know that a prescaler acts like a divider.
We know from looking at the oscilloscope that our timer is at 6 milliseconds vs 1 millisecond.
Therefore our clock is occurring 1/6th as often as we expect.

> **10.3.3** After any Reset, CLK_MAIN is provided by the 16/20 MHz Oscillator (OSC20M) and with a prescaler factor of 6.

Now that we know what the problem is, how do we fix it?

We could adjust all of our code to compensate.
However, it is better to know the actual `F_CPU` since we use it for lots of calculations.
We want to disable that default prescaler.

More reading _(yeah, its pretty dense)_ and we learn there is a control bit to turn off the main clock prescaler.

```C
    CLKCTRL.MCLKCTRLB &= ~(CLKCTRL_PEN_bm); // clear prescaler enable bit
```

That was not so bad. So we are done? Sorry, no.

> **10.3.5** This peripheral has registers that are under Configuration Change Protection (CCP). To write to these registers, a special key value must first be written to the CPU_CCP register, followed by any change to the protected bits. These two steps must occur within four CPU instructions.

The final solution is:

```C
void cpu_disable_clock_prescaler(void) {
    // Disable the CLK_PER prescaler

    CPU_CCP = CCP_IOREG_gc;                 // unlock protection (this is time critical)
    CLKCTRL.MCLKCTRLB &= ~(CLKCTRL_PEN_bm); // clear prescaler enable bit
}
```

If we add this code before we setup our timer, then the main clock is at the 20MHz we expect!

**Exercise:** Update `main.c` to add the `cpu_disable_clock_prescaler()` function and call it as the first step in `main()`.

The LED should now blink on for one second and then off for one second, repeating.

_Code size: 362 bytes_


## Optional Exercises

Here are some optional exercises to try:

- Program the timer to blink the second LED in the top row opposite the first LED so they form a 'wig-wag'.
  - Is only one LED on at a time?

## Next ...

Once you have completed lesson 2 and are able to turn an LED on and off with a timer,
continue on to [Lesson 3](lesson3.md) or return to the [syllabus](syllabus.md).
