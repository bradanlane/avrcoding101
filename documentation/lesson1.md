# AVR Coding 101: Lesson 1

<table border="0"><tr><td width="48%">
The goal of lesson 1 is to become familiar with the AVR microcontroller programming style.
By the end of this lesson, you should have accomplished the following:

- Understand _a little bit about_ microcontroller memory layout
- Learn how PORT and PIN assignments are used for output and input
- Turn an LED ON/OFF using the button
- Compile and upload your code to the **ACK1**
</td><td width="48%" align="center">
<a href="https://youtu.be/JSKRqhHLsZo" target="_blank"><img src="files/avrcoding101_lesson1.jpg" alt="AVR Coding 101 - Lesson 1"></a>
<br/>
<i>Companion Video of Lesson 1<br> (click to watch)</i>
<br/><br/>
</td></tr></table>

[[_TOC_]]

## Understanding AVR Syntax

_For extended detail, please refer to [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf) - specifically section 5 (I/O Multiplexing Considerations) and then section 16 (PORT I/O Configuration)._

### Microcontroller Memory Map

The AVR microcontroller used on the **ACK1** is an 8-bit processor.
All of the interactions with the hardware are through 8-bit data locations.
For the general purpose I/O pins used in this lesson, the data locations are called registers.
These registers have physical addresses within the memory of the microcontroller located near the start of the memory address space.

<div align="center">![Memory Map](files/memory.jpg)<br/>
<i>breakdown of the address map is located in Section 7 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)</i>
<br/><br/></div>

In addition to the registers, there are several other features of the microcontroller which have fixed assigned locations.
All of these refer to fixed physical locations in the memory map of the microcontroller.

>_In C programming, structures allow programmers to combine basic data types together to create user-defined data types. A structure provides a single name for a block of memory. The individual elements of a structure are referenced relative to the structure reference. For example, if we have a structure named `PERSON` and it has `NAME` and `AGE` as elements, then these are accessed as `PERSON.NAME` and `PERSON.AGE`._

As a developer, you are not required to know the memory mapped locations.
To make coding easier, the manufacturer has provided a header file will labels, names of variables, and definitions of data structures.
You will use the names defined in the header file specific to the microcontroller.
The method of coding used in the lessons uses these memory location names, labels, and definitions.

**FYI:** The memory locations and their correct values are located in a header specific to the microcontroller.
The **ACK1** uses an ATtiny1616. The header file is called `iotn1616.h`.
This file is part of the AVR tools installed automatically by PlatformIO.
You do not need to explicitly include this file.
This correct header file for the microcontroller is automatically loaded whenever you use any of AVR header files.

### PORTs and PINs

This lesson focuses on general purpose I/O through the physical pins on the microcontroller.

The I/O pins are organized into groups of 8.
Each group is identified as a PORT or _peripheral register_.

**Note:** For the purposes of these lessons, the use of `PORTx` means it is applicable to any of the available PORTs, while `PORTA` means it is specific to the `A` port.
Similarly, the use of `PINn` is a generic term to represent any pin, while `PIN3` is a specific pin on a given PORT.

Depending on the size of the AVR microcontroller, there will be multiple PORT definitions.
The ATtiny1616 used on the **ACK1** has `PORTA`, `PORTB`, and `PORTC`.
A PORT may have up to 8 pins but not all PORTs will have all their pins available. Some pins are reserved.
For example, `PORTA` has 8 pins but only 7 are general purpose I/O pins. `PIN0` of `PORTA` is reserved for programming the microcontroller.

Each pin of a PORT has a direction (IN or OUT) which is controlled by its _direction_ register, `PORTx.DIR`.

Output pins may be either HIGH _(have voltage)_ or LOW _(act as GND)_ which is controlled by its _output_ register, `PORTx.OUT`.

Input pins have a state which is either HIGH _(have a voltage)_ or LOW _(zero voltage)_ which is read from its _input_ register, `PORTx.IN`.

## Using PORTs and PINs

Here is an excerpt from the [Getting Started with GPIO](https://ww1.microchip.com/downloads/en/Appnotes/90003229A.pdf) guide from Microchip&reg;.

<div align="center">![PORTs and PINs](files/port_registers.jpg)</div>

To affect a single pin of a PORT, it needs to be set to 0 or 1.

```
    PORTA.DIR = 1;
```

However, this simple assignment affects the whole PORT and thus all of the pins in the register.
This code sets the entire _Direction_ register of `PORTA` to be equal to `1`.
The assignment is an 8-bit value so another way to look at this is to look at it's 8-bit binary representation, `0b00000001`.
The bits of the register correspond to the PINs - i.e. `PIN7`, `PIN6`, ... `PIN0`.
Therefore, the code has set `PIN0` equal to `1` and all the other pins equal to `0`.
This isn't what was intended. We only want to change a single pin.

Since the register is a series of bits, then to affect a single pin we need to use _bitwise_ operations.
Without getting too far into the weeds, here are the basic operations:

| Operator | Description | Example |
|:--------:|:------------|-------:|
|    | _all examples use the following values for `A` and `B`_ | `A = 00110011`<br> `B = 01010101` |
| &  | AND - a bit is in the result if it exists in both operands  | `A & B = 00010001`  |
| \| | OR -  a bit is in the result if it exists in either operand  | `A \| B = 01110111`  |
| ~  | One's Complement - has the effect of 'flipping' all of the bits  | `~A = 11001100`  |
| >> | Left Shift - value is moved left by the number of bits specified by the right operand  | `A << 3 = 10011000` |
| << | Right Shift - value is moved right by the number of bits specified by the right operand  | `B >> 5 = 00000010` |

**NOTE:** If you want to get more background onto bitwise operations, take a look at this [tutorial](https://www.tutorialspoint.com/cprogramming/c_bitwise_operators.htm).

The two most used bitwise operations will be `OR` and `AND NOT`.
The `OR` will set bit in an existing value. The `AND NOT` will clear a bit in an existing value.

<div align="center">![Bitwise OR and ~AND](files/bitwise_animation.gif)<br/><br/>
</div>

**Exercise:** Using `A = 00001100` and `B = 00000010` compute `A \| B`. Next compute `A & (~B)`.

Thankfully, the AVR headers provide definitions to make this a bit easier to read.
For each pin, the headers define `PIN1_bm` _(a bit mask)_ and `PIN1_bp` _(a bit's position)_.

The example for setting `PIN1` of `PORTA` may now be written as follows _(each pair of lines yield the same result in the final compiled code)_:

```
    // Set PIN1 on PORTA; i.e. configure it for output
    PORTA.DIR |= (1 << PIN1_bp);    // this is shorthand for: PORTA.DIR = PORTA.DIR | (1 << PIN1_bp)
    PORTA.DIR |= PIN1_bm;

    // Set PIN1 on PORTA HIGH; i.e. has a voltage
    PORTA.OUT |= (1 << PIN1_bp);    // this is shorthand for: PORTA.OUT = PORTA.OUT | (1 << PIN1_bp)
    PORTA.OUT |= PIN1_bm;

    // Clear PIN1 on PORTA; i.e. configure it for input
    PORTA.DIR &= ~(1 << PIN1_bp);   // this is shorthand for: PORTA.DIR = PORTA.DIR & ~(1 << PIN1_bp)
    PORTA.DIR &= ~PIN1_bm;
```

Of the two options, the use of `*_bm` is easier to read.

But wait! There is more.

The AVR has PORT registers to separate `DIRSET` _(setting)_ and `DIRCLR` _(clearing)_ bits.
The final example looks like this:

```
    PORTA.DIRSET = PIN1_bm;         // Set PIN1 on PORTA; i.e. configure it for output

    PORTA.OUTSET = PIN1_bm;         // Set PIN1 on PORTA HIGH; i.e. has a voltage
    PORTA.OUTCLR = PIN1_bm;         // Set PIN1 on PORTA LOW; i.e. act as GND

    PORTA.DIRCLR = PIN1_bm;         // Clear PIN1 on PORTA; i.e. configure it for input
```

### The ACK1 (AVR Coding Kit)

The labeling on the **ACK1** use the combined PORT and pin designations of the Microchip &reg; ATtiny1616.
For example, the buzzer is labeled `PB4` which represents `PORTB` and `PIN4`.

The LEDs use `PORTA` and multiple pin assignments.
For brevity, the `PA` prefix has been omitted.
Each LED is connected to two pins.
The two pins are represented by a 2-digit number.
For example, the LED corresponding to `53` uses the `PA5` and `PA3` pins.

**NOTE:** The PORT and PIN syntax is shared across the TinyAVR and AVRx family of microcontrollers.
The concepts and programming from this course will translate to a large family of Microchip &reg; AVR devices.

## Programming with PORTs and PINs

The programming exercise for lesson 1 is to control an LED with a button.

An LED is a diode.

A simple view of a diode is it acts like a one-way value - it will pass electricity one way while blocking it the other.
When an LED passes electricity, it illuminates.
When it blocks electricity, it doesn't illuminate.

The technical term for the positive and negative ends of a diode are call the anode and a cathode respectively.
When sufficient voltage is applied to the anode, and the cathode is connected to GND, the LED will light up.

### Controlling an LED

On the microcontroller, we may set any output pin as either HIGH or LOW.
A pin configured as HIGH provides a voltage. A pin configured as LOW acts as GND.
If we connect an LED to two pins of the microcontroller, setting the direction of both pins as output, and then set the pin connected to the anode as HIGH, the LED will light.

Looking at the **ACK1**, all of the LEDs use `PORTA`.
The upper left LED is labeled `12` which represents `PIN1` and `PIN2`.
Notice the comment at the top of the **ACK1**. The numbers represent the cathode and anode respectively.
To light this LED, the anode, `PIN2`, needs a voltage while the cathode, `PIN1` needs to be GND.

The first step is to set the _direction_ of both pins of the LED as output using `PORTA.DIRSET`.
We can set multiple pins at once by combining the associated bits.
From the table on _bitwise_ operations, bits are combined using the **OR** operation `(PIN1_bm | PIN2_bm)`.

The result is:

```C
void led_setup(void) {
    PORTA.DIRSET = (PIN1_bm | PIN2_bm);     // Set both PIN1 and PIN2 on PORTA as OUTPUT
}
```

Next, we set the output of `PIN2` - aka the anode pin of the LED - to HIGH _(output a voltage)_ using `PORTx.OUTSET`.

The result is:

```C
void led_on(void) {
    PORTA.OUTSET = PIN2_bm;                 // Set PIN2 on PORTA as HIGH to turn the LED on
}
void led_off(void) {
    PORTA.OUTCLR = PIN2_bm;                 // Set PIN2 on PORTA as LOW to turn the LED off
}
void led_toggle(void) {
    PORTA.OUTTGL = PIN2_bm;                 // change PIN2 on PORTA between LOW and HIGH
}
```

Wait?! What is that last one?

Yeah, in addition to `OUTSET` and `OUTCLR`, there is `OUTTGL` to _toggle_ a pin between HIGH and LOW.

While it looks super convenient, it has its limitation because you do not know if it is currently `SET` (_HIGH)_ or `CLR` _(LOW)_.
However, it is handy for debugging as a simple way to toggle and LED every time _something_ happens.

**Exercise:** Edit `main.c` to turn on the first LED (upper left) of the block of LEDs.

#### How to code for the ACK1

Since this is the first coding exercise, here are some reminders from the [pre-requisites](prereq.md) instructions.

The code you will write needs to go in the file named `main.c` located in the `src` folder.

There is already an empty file ready for you to add your code.
The file already has a number of `#include` header files at the top.
Not all of these are needed for lesson 1 but will be needed as the lessons progress.
Within `main.c` there is already the _main_ function - `int main (void)`.
This is called when the microcontroller is powered.

```C
int main (void) {
    // perform one time setup code here
    while (1) {
        // perform repeated code here
    }
}
```

**Exercise:** Edit `main.c` to turn on the first led.

All of your code will either be in the `main()` function or will be called from this function.
You will find comments indicating where you want to put code.
There are two sections.
The first is for any code you only need to run once when the **ACK1** starts.
The second is within a `while (1) { }` loop.
This code will run _forever_.

For this exercise, `led_setup()` and `led_on()` only need to run once so place these function calls in the _setup_ section.

**RTFM:** Write your code in the `main.c` file. The contents of the `lesson*.h` files are for reference so you may compare your results.

Once you have written your code, you need to build it into _firmware_ and then upload it to the **ACK1**.

The build and upload instructions have been covered as part of the the [pre-requisites](prereq.md) instructions. Here is a refresher of the PlatformIO toolbar, located near the bottom left of the VS Code window.

<div align="center">![PlatformIO Toolbar](files/toolbar_diagram.jpg)<br/></div>

### Read a Button

A button is often connected between a pin of the microcontroller and GND.
The pin is checked for its value, either `0` _(LOW)_ or `1` _(HIGH)_.
When the button is pressed, the connected pin value is `0`.

On the **ACK1** there are two buttons.
They are labeled `PB5` and `PC3` corresponding to `PORTB PIN5` and `PORTC PIN3`.

For the pin to change, based on the button, it is configured as _input_.
From the section on [PORT Pin Syntax](#using-ports-and-pins) a pin is configured for input when its location in the direction register `DIR` is cleared i.e. `PORTx.DIRCLR`.

The result is:

```C
void button_setup(void) {
    PORTB.DIRCLR = PIN5_bm;                 // Clear PIN5 on PORTB; i.e. configure it for input
}
```

At this point, the pin is configured for input but it has an unknown state - it is not guaranteed to be HIGH _(having a voltage)_ or LOW _(having zero voltage)_.

The simple solution is to apply a tiny voltage to the pin.
The microcontroller has a built-in solution for this - called a _pull-up_.
A pull-up is a large value resistor connected between the microcontroller's power and the assigned pin.
The large resistance means it is easy to dissipate the voltage.

Each pin within a PORT has a configuration control register.
Unlike the syntax for setting a pin's direction where there is a single bit on the PORT register _(eg: `PORTA.DIRSET = PIN1_bm`)_, there are multiple possible controls for a pin so each pin on on each port has its one control register.
The syntax is `PORTx.PINnCTRL`. The specific example for `PORTB` and `PIN5` becomes `PORTB.PIN5CTRL`.

We insure the default state of the button pin is HIGH by _enabling_ the _Pull-Up_ bit:

```C
void button_setup(void) {
    PORTB.DIRCLR = PIN5_bm;                 // Clear PIN5 on PORTB; i.e. configure it for input
    PORTB.PIN5CTRL = PORT_PULLUPEN_bm;      // enable the pull-up on PIN5 of PORTB
}
```

The button pin is now set for input and has a small pull-up voltage. The button pin default value is now HIGH aka `1`.
When the button is pressed, it connects to GND and that small voltage of the pull-up is dissipated, so the pin value becomes LOW, aka `0`.

The final step is to check the value of the button pin.
From the table on _bitwise_ operations, bits are checked using the **AND** operation `(PORTB.IN & (PIN5_bm))`.
A value of `1` means the button has not been pressed. We are interested it knowing when the button is pressed so we test for a value of `0`.

The result is:

```C

bool button1_pressed(void) {
    // if the value of the button pin is zero it indicates the button is pressed
    if ( (PORTB.IN & (PIN5_bm)) == 0 )
        return true;
    return false;
}
```

#### Control an LED with a Button

**Exercise:** Edit `main.c` to turn on the first LED (upper left) of the block of LEDs when the button is pressed and turn it off when it is not pressed.

You only need to setup the LED pin as _output_ once at the beginning of your code.
Looking at the labels on the **ACK1**, the first led is numbered `12` which means it uses to I/O pins, `PA1` and `PA2`. The `PA2` pin is the anode and is the pin which needs to receive voltage.

You also need to setup the button as _input_ with it's pull-up enabled. This too can be done at the start of your code.

Finally, check the button pin.
If the button pin is LOW aka `0` it means the button is currently pressed.
If the button is pressed, set the LED anode pin HIGH.
If the button is not pressed, then set the LED anode pin LOW.

You want to be able to check the button pin over and over again, forever.
Your _run forever_ code go inside the  `while(1) { ... }` loop.

_Code size: 192 bytes_

## Optional Exercises

Here are some optional exercises to try:

- Program the second button to control the second LED in the top row
  - When you press the first button does only one LED light?
  - When you press the second button does only one LED light?

## Next ...

Once you have completed lesson 1 and are able to turn an LED on and off with a button,
continue on to [Lesson 2](lesson2.md) or return to the [syllabus](syllabus.md).
