# AVR Coding 101 - Lesson 4

<table border="0"><tr><td width="48%">
The goal of lesson 4 is to become familiar with the AVR microcontroller timer TCA0.
By the end of this lesson, you should have accomplished the following:

- Understand how to setup TCA0 to repeat on an interval
- Control the state of an I/O pin on an interval
- Make annoying tones with a piezo buzzer
</td><td width="48%" align="center">
<a href="https://youtu.be/fmKOtuphikQ" target="_blank"><img src="files/avrcoding101_lesson4.jpg" alt="AVR Coding 101 - Lesson 4"></a>
<br/>
<i>Companion Video of Lesson 4<br> (click to watch)</i>
<br/><br/>
</td></tr></table>

[[_TOC_]]

## Understanding Timers and I/O Pins

### Refresher on Timers

From lesson 2 we know that most microcontrollers have more than one timer.
We also know that timers have different resolutions (8-bit timers, 16-bit timers, etc).

Both, the `TCB0` and `TCA0` timers of the ATtiny1616 microcontroller on the **ACK1** are 16 bit.

In lesson 2 we used TCB0 to create a 1-millisecond timer by writing an interrupt service routine (ISR).
In lesson 3 we used that timer an its ISR to control charlieplexed LEDs.

The amount of time it takes to for a timer to reach its interval
is depended on the speed of the CPU (`F_CPU`).
A 16-bit timer which has a maximum value of 65,535 and our microcontroller is operating at 20Mhz.
The maximum duration of the timer is 65535 / 20,000,000 or 0.0033 seconds.

### Timers and Frequencies

A frequency has two equal intervals to form a period. To convert a timer interval to frequency we need two interval events.

If the maximum interval of a 16-bit timer is 65535 then the maximum interval to generate a frequency is 65535 \* 2.
The lowest frequency is generated from the longest interval.
The lowest frequency is 20,000,000 / (65535 \* 2) or 153 Hz.

To have a lower frequency, we need a longer duration.

To solve the duration problem, we need to apply a divider.
If we are using an interrupt service routine, it's easy to code a divider.
If we are not using an interrupt service routine, then we need a hardware divider called a _prescaler_.

### Timers and I/O Pins

A timer has two options for _doing something_.
A timer may be configured to execute code or it may change the state of an I/O pin.

They have a maximum value of 65,535 before resetting to zero.
An advantage of the `TCA0` timer is the range of hardware prescalers.
This timer supports prescalers from 1 through 1024 in multiples of 2.

This lesson will use the `TCA0` timer to generate tones by switching an I/O pin connected to a piezo buzzer.
The result is a square wave.
This takes advantage of the _frequency waveform generation_ timer event 20.3.3.4.2 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf).

But wait!! A timer can not control _just any I/O pin_.
There are specific pins available to each timer.

<div align="center">![PORT Function Multiplexing](files/pin_functions.jpg)<br/>
<i>from the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)</i><br/><br/>
</div>

Using the column for `TCA0` and the column for the 20-pin VQFN used on the **ACK1**, there are several pins available.
The signal pins are labeled `WO0` through `WO5`.
However, `W03`, `W04`, and `W05` are when the timer is used in split mode as two 8-bit timers.
We focus on the pin assignments for `WO0`, `WO1`, and `WO2`.

**Note:** The **ACK1** an this lesson could have used the TCD timer. It was not chosen to avoid pin conflicts using specific pins for SPI. SPI is not part of the AVR Coding 101 lessons but may be covered by a future lesson.

**Disclaimer:** _It's almost impossible to fully understand the requirements and limitations of I/O pins without reading multiple parts of the datasheet. This lesson will not attempt to provide all possible background. Thankfully, the datasheet has nearly 600 pages to get into excruciating detail. If that is not enough, an internet search will turn up a plethora of useful and useless information as well as conflicting and misleading information. You've been warned._

## Programming a Timer

The setup of `TCA0` is very similar to `TCB0`.
The difference is we do not want to use an interrupt service routine.
We only want to control an I/O pin.

We will need to calculate the timer interval and
we will need to configure the timer to change the I/O pin between HIGH and LOW.

### Timer I/O Pins

From Table 5.1 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)

- We want pins we can controlled by `TCA0`.
- We also want to use `TCA0` as a full 16 bit timer _(vs two 8 bit timers)_.
- We want to generate a waveform.

This focuses us on the pin assignments for `WO0`, `WO1`, and `WO2`.

`WO0`, `WO1`, and `WO2` map to `PB0`, `PB1`, and `PB2` respectively.
They alternately map to `PB3`, `PB4`, and `PB5`.
However, we have a couple conflicts to consider:

- The UART _(used in lesson 5)_ uses `PB2` and `PB3`
- The **ACK1** breaks out I2C which uses `PB0` and `PB1`.

That leaves only `PB4` and `PB5`.
These correspond to `WO1` and `WO2`.
The **ACK1** connects `PB4` to the piezo buzzer.

### Configure The TCA0 Timer

We want to control `PB4`. This is available as the _alternate_ pin for `WO1`.
To use the alternate pins, we instruct the microcontroller using it's `PORTMUX` register.

```C
void tone_setup(void) {
    // switch I/O pins from the primary to alternate assignments
    PORTMUX.CTRLC = PORTMUX_TCA01_ALTERNATE_gc;

    // set the piezo buzzer pin to output LOW
    PORTB.DIRSET = PIN4_bm;
    PORTB.OUTCLR = PIN4_bm;
}
```

We want to use the frequency mode of `TCA0`.
To enable the waveform on a pin, we refer to 20.5.2 of the
[datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf).

We want the mode to control `PB4` which is `WO1`.
The `CTRLB` register has bits for enabling the waveform output,
`WO0`, `WO1`, and `WO2` are controlled by `CMP0EN`, `CMP1EN`, and `CMP2EN` respectively.

```C
void tone_setup(void) {
    // switch I/O pins from the primary to alternate assignments
    PORTMUX.CTRLC = PORTMUX_TCA01_ALTERNATE_gc;

    // set the piezo buzzer pin to input LOW to disable it
    PORTB.DIRCLR = PIN4_bm;
    PORTB.OUTCLR = PIN4_bm;

    // use Frequency mode for WO1
    TCA0.SINGLE.CTRLB = TCA_SINGLE_CMP1EN_bm | TCA_SINGLE_WGMODE_FRQ_gc;
}
```

To generate a tone we need to calculate the timer interval and then enable the interrupt event.

Most applications want to code in terms of a frequency.
The conversion from frequency to interval requires the CPU frequency `F_CPU`.
It also need to take into account a waveform changes twice to form its period.

```C
#define FREQUENCY_TO_INTERVAL(n) (((F_CPU) / (n * 2)) - 1) // 16bit values

void tone_on(uint16_t frequency) {
    uint16_t interval = FREQUENCY_TO_INTERVAL(frequency);

    PORTB.DIRSET = PIN4_bm;         // set as output

    TCA0.SINGLE.CMP0 = interval;    // full period

    TCA0.SINGLE.CTRLA = TCA_SINGLE_ENABLE_bm; /* start timer */
}

void tone_off() {
    TCA0.SINGLE.CTRLA &= ~(TCA_SINGLE_ENABLE_bm);   // stop timer
    PORTB.OUTCLR = PIN4_bm;
    PORTB.DIRCLR = PIN4_bm;
}
```


**Exercise:** Edit `main.c` to configure TCA0 to generate a tone when a button is pressed.
Use both buttons.
Configure PB5 button to generate a tone at 200 Hz.
Configure PC3 button to generate a tone at 100 Hz.

Something isn't quite right.

Back about 15 minutes ago with the introduction of
[Timers and Frequencies](#timers-and-frequencies)
we calculated the lowest frequency possible with a 16-bit timer is about 153 Hz.
In order to generate a lower frequency, we need to apply a prescaler.

```C
#define FREQUENCY_TO_INTERVAL(n) (((F_CPU) / (n * 2)) - 1) // 16bit values

#define MINIMUM_FREQUENCY (((F_CPU) / 2) / 65535)

void tone_on(uint16_t frequency) {
    uint8_t prescaler = 0;

    while (frequency < MINIMUM_FREQUENCY) {
        // the prescaler increments in powers of 2
        prescaler++;
        // as the prescaler increases, the effective frequency decreases
        frequency = frequency * 2;
    }

    uint16_t interval = FREQUENCY_TO_INTERVAL(frequency);

    TCA0.SINGLE.CMP0 = interval;

    PORTB.DIRSET = PIN4_bm;         // set as output

    // the prescaler are bits 3,2,1 of the CTRLA register
    TCA0.SINGLE.CTRLA = (prescaler << 1) | TCA_SINGLE_ENABLE_bm; /* start timer */
}
```

**Exercise:** Edit `main.c` to calculate a prescaler when it is needed.

Don't forget, you can include the code from the previous lessons:

```C
#include "cpu.h"
#include "milliseconds.h"
#include "buttons.h"

// ... rest of your code ...

```

**Question:** How accurate in the tone generation?

<details>

<summary><b>Answer ...</b></summary>

The **ACK1** does not include an external oscillator. It is only as accurate as
the internal reference oscillator.
While the AVR microcontrollers have much better oscillators than previous
chips like the ATMega328, it is still not perfect.

Here is the oscilloscope output of the exact **ACK1** used to develop this material.

<div align="center">![Oscilloscope 6ms](files/oscilloscope_0987hz.png){width=48%} ![Oscilloscope 1ms](files/oscilloscope_1000hz.png){width=48%}<br/>
left image is without error correction -- right image is with error correction<br/><br/></div>

Each AVR microcontroller is tested during production.
It's specific oscillator error is measured and stored in a permanent register location.
There are four values stored, one each for the internal oscillator at 16 MHz and 20 Mhz
and when the microcontroller is powered at 3V and at 5V.
These register locations have defined labels in the header file for the chip.

For the **ACK1**, the relevant oscillator error value is `SIGROW_OSC20ERR3V`.
This is a Q1.10 fixed decimal value stored in 8 bits.
We can adjustment our calculated interval to incorporate this calibration error.
It still will not be perfect.
The internal reference oscillator is affected by both voltage and heat.

```C
#define CLK_COMPENSATION(n) ((((uint32_t)(n)) * (1024 + ((int8_t)(SIGROW_OSC20ERR3V)))) / 1024)

    // the correction is applied to the computed interval
    uint16_t interval = FREQUENCY_TO_INTERVAL(frequency);
    interval = CLK_COMPENSATION(interval);
```
</details>


_Code size: 769 bytes_

## Optional Exercises

Here are some optional exercises to try:

- Play the Major scale (C D E F G A B C)
  - Play the next note with each button press
  - Use a timer to play each note in succession
  - Use the buttons to change the direction (ascending and descending) of the notes

## Next ...

Once you have completed lesson 4 and are able to generate a range of tones,
continue on to [Lesson 5](lesson5.md) or return to the [syllabus](syllabus.md).
