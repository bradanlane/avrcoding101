# AVR Coding 101 - Lesson 6

<table border="0"><tr><td width="48%">
The goal of lesson 6 is to become familiar with the AVR microcontroller EEPROM, Signature data, and FUSES.
By the end of this lesson, you should have accomplished the following:

- Read EEPROM bytes using `pymcuprog`
- Read and write bytes of data to EEPROM using code
- Read Signature bytes using `pymcuprog`
- Read the Signature bytes using code
- Read and (if you wre very very very carful) write the FUSE configuration
  - FUSES will be accessed from the terminal using `pymcuprog`
</td><td width="48%" align="center">
<a href="https://youtu.be/3rYbD9xiEbU" target="_blank"><img src="files/avrcoding101_lesson6.jpg" alt="AVR Coding 101 - Lesson 6"></a>
<br/>
<i>Companion Video of Lesson 6<br> (click to watch)</i>
<br/><br/>
</td></tr></table>

[[_TOC_]]

## Understanding EEPROM

Lesson 1 introduced the memory map of the AVR microcontroller.
Figure 6-1 included the memory area for the EEPROM.

The ATtiny1616 has 256 bytes of EEPROM.
EEPROM data is persistent, even through power off/on.
This makes the EEPROM memory very useful for saving configuration or state information.

## Accessing EEPROM

The EEPROM memory locations may be read at anytime.
They may be read from within a program and they may be read
using the `pymcuprog` programmer.

### Access EEPROM with `pymcuprog`

**Exercise:** Read the first 8 bytes of EEPROM.

Open the VS Code terminal using the _square and angle bracket_ icon in the bottom toolbar.
Execute the following command _(substituting the serial port used by your **ACK1**)_.

**Warning:** `pymcuprog` uses the serial connection.
If you have the Serial Monitor connected, `pymcuprog` will fail.
There is a drop-down list of active panels.

```bash
pymcuprog read  -m eeprom -t uart -d attiny1616 -u <PORT> -c 115200 -b 8
```

The default value of uninitialized memory is `0xFF`.

**Exercise:** Set the 2nd byte of EEPROM to 65 and then verify it.

The `pymcuprog` assumes hexadecimal data so `65` is `0x41`.
Execute the following command _(substituting the serial port used by your **ACK1**)_.

```bash
pymcuprog write  -m eeprom -t uart -d attiny1616 -u <PORT> -c 115200 -o 1 -l 0x41

pymcuprog read  -m eeprom -t uart -d attiny1616 -u <PORT> -c 115200 -b 8
```

### Access EEPROM from Code

The code for reading from EEPROM is very simple.
The microcontroller header file provides a label for the start of the EEPROM memory, `EEPROM_START`.
Yes, it is that easy! The only thing to remember is this is a memory location.
To access a memory location we treat it like a _pointer_.

```C
    uint8_t b = 0;
    b = *(uint8_t *)(EEPROM_START);
```

The microcontroller header file also provides a label for the size the EEPROM memory, `EEPROM_SIZE`.

```C
    if ((address >= EEPROM_START) && (address < (EEPROM_START + EEPROM_SIZE))) {
        // valid EEPROM location
    }
}
```

#### Read from EEPROM

To read any valid EEPROM byte, its the combination of the above code.

```C
uint8_t eeprom_get_byte(uint16_t offset) {
    uint8_t b = 0;

    // prevent accessing addresses outside of the available EEPROM space
    if (offset < EEPROM_SIZE) {
        // Read operation will be stalled by hardware if any write is in progress
        b = *(uint8_t *)(EEPROM_START + offset);
    }
    return b;
}
```

#### Write to EEPROM

Writing to EEPROM is _almost_ as easy.

We have performed similar steps in previous lessons.

In lesson 5 where we had check if it was OK to transmit a byte of data using USART.
Here, we need to be sure the microcontroller is ready to save a byte to EEPROM, by testing for `NVMCTRL_EEBUSY_bm`.

In lesson 2 we learned some memory locations are protected and must be unlocked.
The same is true for instructing the microcontroller to perform the _write_ operation
to the physical EEPROM memory.

The _configuration change protection_ is call `CCP` and the change we want to make is
_erase and write_ a byte in physical memory - `NVMCTRL_CMD_PAGEERASEWRITE_gc`.

```C
void eeprom_put_byte(uint16_t relative_addr, uint8_t data) {
    if (relative_addr < EEPROM_SIZE) {

        /* Wait for completion of any previous write */
        while (NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm)
            ;

        // Load in the relevant EEPROM page by writing directly to the memory address
        *(uint8_t *)(EEPROM_START + relative_addr) = data;

        // Unlock self programming and then erase/write the 'page' (one byte)
        CCP = CCP_SPM_gc;
        NVMCTRL.CTRLA = NVMCTRL_CMD_PAGEERASEWRITE_gc;
    }
}
```

**Exercise:** Increment the 2nd EEPROM byte with a Button

**Hint:** To increment a value, read the current value when you initialize other data in your program.
When a button is pressed, increment your local variable, then write it out to EEPROM.
Be careful to only perform the action once each time the button is pressed _(like we did with lesson 4 and lesson 5)_.

_Code size: 326 bytes_

You can confirm you have updated EEPROM using `pymcuprog`.

```bash
pymcuprog read  -m eeprom -t uart -d attiny1616 -u <PORT> -c 115200 -b 8
```

Don't forget, you can include the code from the previous lessons:

```C
#include "cpu.h"
#include "milliseconds.h"
#include "buttons.h"

// ... rest of your code ...

```

## Understanding the Signature Row

Lesson 1 introduced the memory map of the AVR microcontroller.
Figure 6-1 included the memory area for the NVM I/O Registers and Data.
The microcontroller signature bytes are located in this area.

The microcontroller's _device type_ and its _unique serial number_
are located within the signature bytes.
The serial number is handy for situations where it's important to
identify an individual microcontroller _(or the device it is installed in)_ .

## Accessing the Signature Row

The Signature row bytes may be read at anytime.
The location of these bytes if fixed and the location is given a label by the
microcontroller header file.
The bytes are labeled `SIGROW_SERNUM0` through `SIGROW_SERNUM9`.

They may be read from within a program and they may be read
using the `pymcuprog` programmer.

### Access the Serial Number Row with `pymcuprog`

**Exercise:** Read the 10 byte serial number.
The serial number starts at the 4th byte of the signature row.

Open the VS Code terminal using the _square and angle bracket_ icon in the bottom toolbar.
Execute the following command _(substituting the serial port used by your **ACK1**)_.

The first command will read the device type.
All **ACK1** boards use the ATtiny1616 which has a device type of `1E9421`.

The second command will read the unique serial number of your specific ATtiny1616.

```bash
pymcuprog read -m signatures -t uart -d attiny1616 -u <PORT> -c 115200 -b 3
pymcuprog read -m signatures -t uart -d attiny1616 -u <PORT> -c 115200 -o 3 -b 10
```

### Access Serial Number from Code

The code for reading the serial number is very simple.
The microcontroller header file provides labels for each byte of the serial number.
The bytes of the serial number are memory locations.
This means - in addition to use each of the predefined labels - its is also
possible to treat the entire serial number as a buffer and read from it sequentially.

To access a memory locations we treat the first location like a _pointer_.

```C
    const uint8_t* serno = &(SIGROW_SERNUM0);

    uint8_t byte;
    // read and output the serial number
    for (int i = 0; i < 10; i++) {
        byte = serno[i];

        // ... do something with each byte
    }
```

**Exercise:** Read the serial number and output the hex values.
You only need to simple TX-only code from lesson 5.

**Hint:** Here is a simple method of converting an 8-bit hexadecimal number to two ascii characters.

```C
    uint8_t byte = 0x3A;
    uint8_t high, low;

    high = (byte >> 4) & 0x0F;
    low = (byte) & 0x0F;

    if (high > 9)
        high = 'A' + (high - 10);
    else
        high = '0' + (high);

    if (low > 9)
        low = 'A' + (low - 10);
    else
        low = '0' + (low);

    // you can now output high and low
```

_Code size: 412 bytes_

Don't forget, you can include the code from the previous lessons:

```C
#include "cpu.h"
#include "milliseconds.h"
#include "buttons.h"
#include "usart_tx_only.h"

// ... rest of your code ...

```

## Understanding FUSES

Fuses are part of the nonvolatile memory and hold the device configuration.

Lesson 1 introduced the memory map of the AVR microcontroller.
Figure 6-1 included the memory area for the NVM I/O Registers and Data.
The microcontroller configuration fuses are located in this area.

Some fuses are benign while others can **brick** your microcontroller.

<div align="center">**!! EXTREME CARE SHOULD BE TAKEN WHEN MODIFYING FUSES !!**</div>

<div align="center">![AVR FUSE Memory](files/fuses.jpg)<br>
The fuses are detained in section 6.10.4 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf)<br/><br/></div>

| Fuse | Description |
|------|-------------|
|WDTCFG|Watchdog timer configuration (default disabled)|
|BOD|Brownout (low voltage) detection (default disabled)|
|OSCCFG|Oscillator configuration (default frequency is 20M)|
|TCD0CFG|Timer counter configuration (enable/disable pins)|
|SYSCFG0|System configuration (_be vehwy vehwy cwareful_)|
|SYSCFG1|System configuration (startup delay; rarely changed)|
|APPEND|Code location (rarely changed)|
|BOOTEND|Boot end (only change if there is a bootloader)|
|LOCKBIT|Lock bits (a special key to lock memories)|

Of the above fuses, the most useful and also one of the most dangerous, is `SYSCFG0`.
The first bit of this fuse controls whether or not the EEPROM is erased when
the microcontroller is programmed. It can be useful to preserve the EEPROM across program flashing.

The `SYSCFG0` fuse also contains bits for the function of the `RSTPINCFG`.
This controls the function of the UPDI pin.
Changing this can render the microcontroller **unprogrammable**.
A special _high voltage_ programmer is required to recover the microcontroller.

**Tip:** The safest means of changing the EEPROM erase bit within the `SYSCFG0` fuse
is to first read the fuse value. Next, calculate the value with the EEPROM bit set/cleared as needed.
Finally, write the new fuse value back to the microcontroller.

## Accessing FUSES

The fuses are accessed through the UPDI programming interface.
For the **ACK1**, we are using the `pymcuprog` programmer.

### Access FUSES with `pymcuprog`

**Exercise:** Read the fuses from the microcontroller.

Open the VS Code terminal using the _square and angle bracket_ icon in the bottom toolbar.
Execute the following command _(substituting the serial port used by your **ACK1**)_.

```bash
pymcuprog read  -m fuses -t uart -d attiny1616 -u <PORT> -c 115200
```

Compare the fuse values against the information in section 6.10.4 of the [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf).

**Exercise:** Read the `SYSCFG0` fuse. The `SYSCFG0` is at offset 5.

```bash
pymcuprog read  -m fuses -t uart -d attiny1616 -u <PORT> -c 115200 -o 5 -b 1
```

**Optional Exercise:** Disable EEPROM erase.
If the value from reading the `SYSCFG0` fuse is 0xF6,
and the bit for EEPROM erase/save is 0x01, then
the new value would be 0xF7.

```bash
pymcuprog write -m fuses -t uart -d attiny1616 -u <PORT> -c 115200 -o 5 -l 0xF7
```

## Optional Exercises

Here are some optional exercises to try:

- Store a value in first byte if EEPROM, program the ACK1, and verify if the EEPROM data persisted
  - set the the EESAVE bit of the SYSCFG0 fuse to '1' repeat the test
  - set the the EESAVE bit of the SYSCFG0 fuse to '0' repeat the test

## Next ...

Once you have completed lesson 6 and are able to
store and retrieve data from EEPROM,
read the serial number, and read and update FUSES.

Take a bow. You have completed all of the lessons in **AVR Coding 101**.

You may be interested in the full [Demo](include/demo.h) which puts everything together.
This is the code that was on your **ACK1** when it was shipped.

Now return to the [syllabus](syllabus.md).
