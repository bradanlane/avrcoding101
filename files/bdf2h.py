from bdfparser import Font
FONTNAME = '100LX_5X7.bdf'
font = Font(FONTNAME)


print("#ifndef __FONT_H")
print("#define __FONT_H")

print("// %s global size is %d x %x pixels with %d glyphs" % (FONTNAME, font.headers['fbbx'], font.headers['fbby'], len(font)))
print("// %s local  size is %d x %x pixels with %d glyphs" % (FONTNAME, font.headers['fbbx']-1, font.headers['fbby']-1, len(font)))
print()
print("#define FONT7X5")
print()
print("// font data optimized for the ACK LED layout, not storage size")
print("// each character is 7 bytes where 5 bits or each byte are used")
print()

# =================================

print ("const uint8_t font7x5[] = {")

for glyph in font.iterglyphs():
    if glyph.cp() < 32:
        continue
    bitmap = glyph.draw().todata(5)
    bytes_string = ""
    for i in range(7):
        byte = bitmap[i]
        byte = int('{:08b}'.format((byte<<1))[::-1], 2)
        bytes_string += ("0x%02X" % (byte)) if (i == 0) else (", 0x%02X" % (byte))

    print("\t/* %03d %s */  %s," % (glyph.cp(), glyph.chr(), bytes_string))

print("};")

print()
print()

print("#define FONT5X7")
print()
print("// font data optimized for the SPI display")
print("// each character is 5 bytes where 7 bits or each byte are used")
print()

print ("const uint8_t font5x7[] = {")

for glyph in font.iterglyphs():
    if glyph.cp() < 32:
        continue

    bitmap = glyph.draw().todata(5)
    bytes = [0 for i in range(6)]

    for r in range(7):
        byte = bitmap[r]
        byte = int('{:08b}'.format((byte<<1))[::-1], 2)
        for c in range(6):
            bytes[c] = (bytes[c] << 1) | ((byte >> c) & 0x01)

    bytes_string = ""
    for c in range(1,6):
        bytes_string += ("0x%02X" % (bytes[c])) if (c == 1) else (", 0x%02X" % (bytes[c]))

    print("\t/* %03d %s */  %s," % (glyph.cp(), glyph.chr(), bytes_string))

print("};")

print()
print("#endif // __FONT_H")
